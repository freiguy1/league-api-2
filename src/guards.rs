use diesel::pg::PgConnection;
use r2d2_diesel::ConnectionManager;
use rocket::http::Status;
use rocket::request::{self, FromRequest};
use rocket::{Request, State, Outcome};
use std::ops::Deref;
use std::sync::{Arc, Mutex};

use config::Config as LeagueConfig;
use league_db::repo::{TeamRepo, LeagueRepo, TeamEventRepo};
use util::Cache as LeagueCache;

// Connection request guard type: a wrapper around an r2d2 pooled connection.
pub struct DbConn(pub ::r2d2::PooledConnection<ConnectionManager<PgConnection>>);

/// Attempts to retrieve a single connection from the managed database pool. If
/// no pool is currently managed, fails with an `InternalServerError` status. If
/// no connections are available, fails with a `ServiceUnavailable` status.
impl<'a, 'r> FromRequest<'a, 'r> for DbConn {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<DbConn, ()> {
        let pool = request.guard::<State<::r2d2::Pool<ConnectionManager<PgConnection>>>>()?;
        match pool.get() {
            Ok(conn) => Outcome::Success(DbConn(conn)),
            Err(_) => Outcome::Failure((Status::ServiceUnavailable, ()))
        }
    }
}

// For the convenience of using an &DbConn as an &PgConnection.
impl Deref for DbConn {
    type Target = PgConnection;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub struct Config<'r>(pub &'r LeagueConfig);

impl<'a, 'r> FromRequest<'a, 'r> for Config<'r> {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Config<'r>, ()> {
        request.guard::<State<LeagueConfig>>()
            .map(|c| Config(c.inner()))
    }
}

impl<'r> Deref for Config<'r> {
    type Target = LeagueConfig;

    fn deref(&self) -> &Self::Target {
        self.0
    }
}

pub struct Cache(pub Arc<Mutex<LeagueCache>>);

impl<'a, 'r> FromRequest<'a, 'r> for Cache {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Cache, ()> {
        request.guard::<State<Arc<Mutex<LeagueCache>>>>()
            .map(|c| Cache(c.inner().clone()))
    }
}

impl Deref for Cache {
    type Target = Arc<Mutex<LeagueCache>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}


pub struct UserOptional(pub Option<i32>);

impl<'a, 'r> FromRequest<'a, 'r> for UserOptional{
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<UserOptional, ()> {
        let user_id = get_user_from_request(request);

        Outcome::Success(UserOptional(user_id))
    }
}

impl Deref for UserOptional {
    type Target = Option<i32>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub struct UserRequired(pub i32);

impl<'a, 'r> FromRequest<'a, 'r> for UserRequired {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<UserRequired, ()> {
        match get_user_from_request(request) {
            Some(user_id) => Outcome::Success(UserRequired(user_id)),
            None => Outcome::Failure((Status::Forbidden, ()))
        }
    }
}

impl Deref for UserRequired {
    type Target = i32;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub struct LeagueAdmin(pub i32);

impl<'a, 'r> FromRequest<'a, 'r> for LeagueAdmin {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<LeagueAdmin, ()> {
        let league_id = match get_id_from_request("league_id", request) {
            Some(id) => id,
            None => {
                println!("For some reason LeagueAdmin was used but no league_id in url");
                return Outcome::Failure((Status::ServiceUnavailable, ()));
            }
        };

        match is_user_league_admin(request, league_id, None) {
            Outcome::Success(user_id) => Outcome::Success(LeagueAdmin(user_id)),
            Outcome::Failure((e, _)) => Outcome::Failure((e, ())),
            Outcome::Forward(f) => Outcome::Forward(f)
        }
    }
}

impl Deref for LeagueAdmin {
    type Target = i32;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub struct LeagueAdminForTeam(pub i32);

impl<'a, 'r> FromRequest<'a, 'r> for LeagueAdminForTeam {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<LeagueAdminForTeam, ()> {
        let team_id = match get_id_from_request("team_id", request) {
            Some(id) => id,
            None => {
                println!("For some reason LeagueAdminForTeam was used but no team_id in url");
                return Outcome::Failure((Status::ServiceUnavailable, ()));
            }
        };

        let pool = request.guard::<State<::r2d2::Pool<ConnectionManager<PgConnection>>>>()?;
        let conn = match pool.get() {
            Ok(conn) => DbConn(conn),
            Err(_) => return Outcome::Failure((Status::InternalServerError, ()))
        };

        let league_id: i32;
        {
            let team_repo = TeamRepo::new(&conn);
            let team = match team_repo.get_by_id(team_id) {
                Ok(Some(t)) => t,
                Ok(None) => return Outcome::Failure((Status::Forbidden, ())),
                Err(_) => return Outcome::Failure((Status::InternalServerError, ()))
            };
            league_id = team.league_id;
        }

        match is_user_league_admin(request, league_id, Some(conn)) {
            Outcome::Success(user_id) => Outcome::Success(LeagueAdminForTeam(user_id)),
            Outcome::Failure((e, _)) => Outcome::Failure((e, ())),
            Outcome::Forward(f) => Outcome::Forward(f)
        }
    }
}

impl Deref for LeagueAdminForTeam {
    type Target = i32;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub struct LeagueAdminForTeamEvent(pub i32);

impl<'a, 'r> FromRequest<'a, 'r> for LeagueAdminForTeamEvent {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<LeagueAdminForTeamEvent, ()> {
        let team_event_id = match get_id_from_request("team_event_id", request) {
            Some(id) => id,
            None => {
                println!("For some reason LeagueAdminForTeamEvent was used but no team_event_id in url");
                return Outcome::Failure((Status::ServiceUnavailable, ()));
            }
        };

        let pool = request.guard::<State<::r2d2::Pool<ConnectionManager<PgConnection>>>>()?;
        let conn = match pool.get() {
            Ok(conn) => DbConn(conn),
            Err(_) => return Outcome::Failure((Status::InternalServerError, ()))
        };

        let league_id: i32;
        {
            let team_event_repo = TeamEventRepo::new(&conn);
            let team_event = match team_event_repo.get_single(team_event_id) {
                Ok(Some(t)) => t,
                Ok(None) => return Outcome::Failure((Status::Forbidden, ())),
                Err(_) => return Outcome::Failure((Status::InternalServerError, ()))
            };
            league_id = team_event.league_id();
        }

        match is_user_league_admin(request, league_id, Some(conn)) {
            Outcome::Success(user_id) => Outcome::Success(LeagueAdminForTeamEvent(user_id)),
            Outcome::Failure((e, _)) => Outcome::Failure((e, ())),
            Outcome::Forward(f) => Outcome::Forward(f)
        }
    }
}

impl Deref for LeagueAdminForTeamEvent {
    type Target = i32;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

// HELPER METHODS

fn is_user_league_admin(request: &Request, league_id: i32, conn: Option<DbConn>) -> request::Outcome<i32, ()> {
    let conn = if let Some(c) = conn {
        c
    } else {
        let pool = request.guard::<State<::r2d2::Pool<ConnectionManager<PgConnection>>>>()?;
        match pool.get() {
            Ok(conn) => DbConn(conn),
            Err(_) => return Outcome::Failure((Status::InternalServerError, ()))
        }
    };

    let user_id = match get_user_from_request(request) {
        Some(user_id) => user_id,
        None => return Outcome::Failure((Status::Forbidden, ()))
    };

    let league_repo = LeagueRepo::new(&conn);

    let league = match league_repo.get_single(league_id) {
        Ok(Some(l)) => l,
        Ok(None) => return Outcome::Failure((Status::Forbidden, ())),
        Err(_) => return Outcome::Failure((Status::InternalServerError, ()))
    };

    if league.administrators.iter().any(|a| a.user_id == user_id) {
        Outcome::Success(user_id)
    } else {
        Outcome::Failure((Status::Unauthorized, ()))
    }
}

fn get_id_from_request(name: &'static str, request: &Request) -> Option<i32> {
    let segment = format!("<{}>", name);
    request.route()
        .and_then(|r| r.uri.segments().filter(|s| s.ends_with(">")).position(|s| s == &segment))
        .and_then(|i| request.get_param::<i32>(i).ok())
}

fn get_user_from_request(request: &Request) -> Option<i32> {
    let config = request.guard::<State<LeagueConfig>>()
        .map(|c| c.inner()).unwrap();

    request.headers().get_one("Authorization")
        .and_then(|s| s.split_whitespace().nth(1))
        .and_then(|t| ::auth::user_id_from_token(t, &config.secret))
}
