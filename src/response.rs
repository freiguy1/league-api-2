use rocket::response::{Response, Responder};
use rocket::http::{Status, Header};
use rocket_contrib::Json;

pub enum LeagueResponse<R> {
    Success(R),
    AddToken(String, R),
    BadRequest(Vec<String>),
    NotFound
}

impl<'r, R: Responder<'r>> LeagueResponse<R> {
    pub fn success(responder: R) -> Self {
        LeagueResponse::Success(responder)
    }

    pub fn bad_request<S: Into<String>>(errors: Vec<S>) -> Self {
        let errors = errors.into_iter().map(|e| e.into()).collect();
        LeagueResponse::BadRequest(errors)
    }

    pub fn not_found() -> Self {
        LeagueResponse::NotFound
    }

    pub fn add_token(token: String, responder: R) -> Self {
        LeagueResponse::AddToken(token, responder)
    }
}

impl <'r, R: Responder<'r>> Responder<'r> for LeagueResponse<R> {
    fn respond_to(self, req: &::rocket::request::Request) -> Result<Response<'r>, Status> {
        match self {
            LeagueResponse::Success(responder) => {
                Response::build()
                    .merge(responder.respond_to(req)?)
                    .ok()
            },
            LeagueResponse::AddToken(token, responder) => {
                let header = Header::new("Authorization", format!("Bearer {}", token));
                Response::build()
                    .merge(responder.respond_to(req)?)
                    .header(header)
                    .ok()
            }
            LeagueResponse::BadRequest(errors) => {
                let responder = Json(json!(errors));
                Response::build()
                    .status(Status::BadRequest)
                    .merge(responder.respond_to(req)?)
                    .ok()
            },
            LeagueResponse::NotFound => {
                let responder = Json(json!({"message": "item not found"}));
                Response::build()
                    .status(Status::NotFound)
                    .merge(responder.respond_to(req)?)
                    .ok()
            }
        }
    }
}
