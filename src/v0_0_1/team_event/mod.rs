use rocket::Rocket;

mod contract;
mod get_all_by_league;
mod get_all_by_team;
mod get_by_id;
mod create_many;
mod create;
mod delete_many;
mod delete;
mod update;

mod validator;

pub fn init_routes(rocket: Rocket) -> Rocket {
    let routes = routes![
        get_all_by_league::handle,
        get_all_by_team::handle,
        create_many::handle,
        create::handle,
        delete_many::handle,
        get_by_id::handle,
        delete::handle,
        update::handle
    ];
    rocket.mount(&format!("/{}/", super::VER), routes)
}