use rocket::response::content;

use error::Error;
use guards::{DbConn, LeagueAdminForTeamEvent, Cache};
use league_db::repo::{TeamEvent,TeamEventRepo};
use response::LeagueResponse;

type Response = Result<LeagueResponse<content::Json<&'static str>>, Error>;

#[delete("/team_events/<team_event_id>")]
pub fn handle(team_event_id: i32,
              conn: DbConn,
              cache: Cache,
              _user_id: LeagueAdminForTeamEvent) -> Response {
    let team_event_repo = TeamEventRepo::new(&conn);

    let dao_to_delete = team_event_repo.get_single(team_event_id)?.expect("Guard assures team event exists");
    let league_id = dao_to_delete.league_id();
    let existing_team_events = team_event_repo.get_by_league_ids(&vec![league_id])?;

    match dao_to_delete {
        TeamEvent::Matchup(ref m) => {
            ::util::record::invalidate_records_for_team_id(m.home_team_id, cache.clone());
            ::util::record::invalidate_records_for_team_id(m.away_team_id, cache.clone());
        },
        TeamEvent::Bye(ref b) =>
            ::util::record::invalidate_records_for_team_id(b.team_id, cache.clone())
    }

    if any_team_event_has_rteid(&existing_team_events, team_event_id) {
        return Ok(LeagueResponse::bad_request(vec!["This team_event was rescheduled and is linked to another team_event. Delete linked team_event first."]))
    }

    team_event_repo.delete_many(&vec![team_event_id])?;

    let json = content::Json("{ \"success\": true }");
    Ok(LeagueResponse::success(json))
}

fn any_team_event_has_rteid(team_events: &Vec<TeamEvent>, team_event_id: i32) -> bool {
    let mut result = false;
    for te in team_events.iter() {
        result = result | match te {
            &TeamEvent::Matchup(ref m) => m.rescheduled_team_event_id.map(|r| r == team_event_id).unwrap_or(false),
            _ => false
        };
    }
    result
}


