use chrono::NaiveTime;
use rocket::response::content;
use rocket_contrib::Json;

use super::contract::CreateTeamEventsRequestItem;
use error::Error;
use guards::{DbConn, LeagueAdmin, Cache};
use league_db::repo::{NewMatchup, NewBye, NewGame, TeamEventRepo, LeagueRepo, TeamRepo};
use response::LeagueResponse;

type Response = Result<LeagueResponse<content::Json<&'static str>>, Error>;

#[post("/leagues/<league_id>/team_events/all", data = "<create_team_events_request>")]
pub fn handle(league_id: i32,
              create_team_events_request: Json<Vec<CreateTeamEventsRequestItem>>,
              conn: DbConn,
              cache: Cache,
              _user_id: LeagueAdmin) -> Response {
    let team_event_repo = TeamEventRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);
    let reference = ::util::Reference::new(cache.clone(), &conn);
    let statuses = reference.matchup_statuses();

    let league = league_repo.get_single(league_id)?.expect("Guard guarantees league exists");

    let teams = team_repo.get_by_league_id(league_id)?;
    let existing_team_events = team_event_repo.get_by_league_ids(&vec![league_id])?;

    if existing_team_events.len() > 0 {
        return Ok(LeagueResponse::bad_request(vec!["Cannot create team events if some already exist"]));
    }

    let validation_errors = validate(&create_team_events_request,
                                     &statuses,
                                     teams.iter().map(|t| t.id).collect(),
                                     league.matchup_locations.iter().map(|l| l.id).collect());

    if !validation_errors.is_empty() {
        return Ok(LeagueResponse::bad_request(validation_errors));
    }

    for create_team_events_request_item in create_team_events_request.into_inner().into_iter() {
        if create_team_events_request_item.home_team_id.is_some() {
            let dao = build_new_matchup_dao(league_id, create_team_events_request_item, &statuses);
            team_event_repo.create_matchup(&dao)?;
        } else {
            let dao = build_new_bye_dao(league_id, create_team_events_request_item);
            team_event_repo.create_bye(&dao)?;
        };
    }

    let json = content::Json("{ \"success\": true }");
    Ok(LeagueResponse::success(json))
}

fn build_new_matchup_dao(league_id: i32, req: CreateTeamEventsRequestItem, statuses: &Vec<::util::KeyValue>) -> NewMatchup {
    let games: Vec<NewGame> = if let Some(ref games) = req.games { 
        games.iter().map(|g| NewGame {
            sequence: g.sequence,
            home_team_score: g.home_team_score,
            away_team_score: g.away_team_score,
            notes: g.notes.clone()
        }).collect()
    } else {
        Vec::new()
    };

    NewMatchup {
        datetime: req.datetime.unwrap().naive_utc(),
        matchup_location_id: req.location_id.unwrap(),
        league_id: league_id,
        home_team_id: req.home_team_id.unwrap(),
        away_team_id: req.away_team_id.unwrap(),
        matchup_status_id: statuses.iter().filter(|s| s.value.to_lowercase() == req.status.as_ref().unwrap().to_lowercase()).next().unwrap().key,
        rescheduled_team_event_id: None,
        notes: req.notes.clone(),
        games: games
    }
}

fn build_new_bye_dao(league_id: i32, req: CreateTeamEventsRequestItem) -> NewBye {
    NewBye {
        datetime: req.bye_date.unwrap().and_time(NaiveTime::from_hms(0, 0, 0)),
        league_id: league_id,
        team_id: req.bye_team_id.unwrap(),
        notes: req.notes.clone()
    }
}

fn validate(req: &Vec<CreateTeamEventsRequestItem>,
            statuses: &Vec<::util::KeyValue>,
            team_ids: Vec<i32>,
            location_ids: Vec<i32>) -> Vec<String> {
    req.iter().flat_map(|i| super::validator::validate(
        i,
        statuses,
        &team_ids,
        &location_ids,
        &vec![], // no existing team events
        None)).collect()
}

