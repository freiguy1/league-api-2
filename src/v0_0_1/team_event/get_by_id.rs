use chrono::{DateTime, Utc};
use rocket_contrib::Json;

use error::Error;
use super::contract::{Game, GetTeamEventResponse};
use league_db::repo::{TeamEventRepo, TeamEvent, Matchup, Bye};
use guards::{DbConn, Cache};
use response::LeagueResponse;

type Response = Result<LeagueResponse<Json<GetTeamEventResponse>>, Error>;

#[get("/team_events/<team_event_id>")]
pub fn handle(team_event_id: i32, conn: DbConn, cache: Cache) -> Response {
    let team_event_repo = TeamEventRepo::new(&conn);
    let reference = ::util::Reference::new(cache.clone(), &conn);
    let statuses = reference.matchup_statuses();

    let dao = match team_event_repo.get_single(team_event_id)? {
        Some(te) => te,
        None => return Ok(LeagueResponse::not_found())
    };

    let team_event = match dao {
        TeamEvent::Matchup(m) => build_response_from_matchup(m, &statuses),
        TeamEvent::Bye(b) => build_response_from_bye(b),
    };

    Ok(LeagueResponse::success(Json(team_event)))
}

fn build_response_from_matchup(m: Matchup, statuses: &Vec<::util::KeyValue>) -> GetTeamEventResponse {
    let games = m.games.iter().map(|g| Game {
        id: g.id,
        sequence: g.sequence,
        home_team_score: g.home_team_score,
        away_team_score: g.away_team_score,
        notes: g.notes.clone()
    }).collect();

    GetTeamEventResponse {
        id: m.id,
        notes: m.notes.clone(),
        datetime: Some(DateTime::from_utc(m.datetime, Utc)),
        location_id: Some(m.matchup_location_id),
        status: Some(statuses.iter().filter(|s| s.key == m.matchup_status_id).next().unwrap().value.clone()),
        rescheduled_team_event_id: m.rescheduled_team_event_id,
        home_team_id: Some(m.home_team_id),
        away_team_id: Some(m.away_team_id),
        bye_date: None,
        bye_team_id: None,
        games: Some(games)
    }

}

fn build_response_from_bye(b: Bye) -> GetTeamEventResponse {
    GetTeamEventResponse {
        id: b.id,
        notes: b.notes.clone(),
        datetime: None,
        location_id: None,
        status: None,
        rescheduled_team_event_id: None,
        home_team_id: None,
        away_team_id: None,
        bye_date: Some(b.datetime.date()),
        bye_team_id: Some(b.team_id),
        games: None
    }
}
