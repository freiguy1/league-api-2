use chrono::{DateTime, Utc};
use rocket_contrib::Json;
use std::collections::{HashSet, HashMap};

use error::Error;
use super::contract::{Game, GetTeamEventsResponseItem};
use league_db::repo::{TeamEventRepo, TeamEvent, Matchup, Bye, LeagueRepo, MatchupLocation, TeamRepo};
use guards::{DbConn, Cache};
use response::LeagueResponse;
use stats::record::Record;

type Response = Result<LeagueResponse<Json<Vec<GetTeamEventsResponseItem>>>, Error>;

#[get("/teams/<team_id>/team_events")]
pub fn handle(team_id: i32, conn: DbConn, cache: Cache) -> Response {
    let team_event_repo = TeamEventRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);
    let reference = ::util::Reference::new(cache.clone(), &conn);
    let statuses = reference.matchup_statuses();

    let team_dao = match team_repo.get_by_id(team_id)? {
        Some(t) => t,
        None => return Ok(LeagueResponse::not_found())
    };

    let league_dao = league_repo.get_single(team_dao.league_id)?.unwrap();

    let team_names: HashMap<i32, String> = team_repo.get_by_league_id(team_dao.league_id)?.into_iter()
        .map(|t| (t.id, t.name)).collect();

    let team_ids = team_names.iter().map(|(id, _)| *id).collect();
    let records = ::util::record::get_records_for_team_ids(team_ids, cache.0, &team_event_repo)?;

    let mut team_id_hash_map = HashSet::new();
    team_id_hash_map.insert(team_id);
    let daos = team_event_repo.get_by_team_ids(&team_id_hash_map)?;

    let team_events = build_response(daos, statuses, league_dao.matchup_locations, team_names, records);
    Ok(LeagueResponse::success(Json(team_events)))
}

fn build_response(
    daos: Vec<TeamEvent>,
    statuses: Vec<::util::KeyValue>,
    locations: Vec<MatchupLocation>,
    team_names: HashMap<i32, String>,
    records: HashMap<i32, Record>) -> Vec<GetTeamEventsResponseItem> {
    daos
        .into_iter()
        .map(|te| match te {
            TeamEvent::Matchup(m) => build_response_from_matchup(m, &statuses, &locations, &team_names, &records),
            TeamEvent::Bye(b) => build_response_from_bye(b, &team_names, &records)
        }).collect()
}

fn build_response_from_matchup(
    m: Matchup,
    statuses: &Vec<::util::KeyValue>,
    locations: &Vec<MatchupLocation>,
    team_names: &HashMap<i32, String>,
    records: &HashMap<i32, Record>) -> GetTeamEventsResponseItem {

    let games = m.games.iter().map(|g| Game {
        id: g.id,
        sequence: g.sequence,
        home_team_score: g.home_team_score,
        away_team_score: g.away_team_score,
        notes: g.notes.clone()
    }).collect();

    GetTeamEventsResponseItem {
        id: m.id,
        notes: m.notes.clone(),
        datetime: Some(DateTime::from_utc(m.datetime, Utc)),
        location_id: Some(m.matchup_location_id),
        location: Some(locations.iter().find(|l| l.id == m.matchup_location_id).unwrap().name.clone()),
        status: Some(statuses.iter().filter(|s| s.key == m.matchup_status_id).next().unwrap().value.clone()),
        rescheduled_team_event_id: m.rescheduled_team_event_id,
        home_team_id: Some(m.home_team_id),
        home_team_name: Some(team_names[&m.home_team_id].clone()),
        home_team_record: Some(records.get(&m.home_team_id).map(|r| r.clone().into()).unwrap_or(Record::empty().into())),
        away_team_id: Some(m.away_team_id),
        away_team_name: Some(team_names[&m.away_team_id].clone()),
        away_team_record: Some(records.get(&m.away_team_id).map(|r| r.clone().into()).unwrap_or(Record::empty().into())),
        bye_date: None,
        bye_team_id: None,
        bye_team_name: None,
        bye_team_record: None,
        games: Some(games)
    }

}

fn build_response_from_bye(b: Bye, team_names: &HashMap<i32, String>, records: &HashMap<i32, Record>) -> GetTeamEventsResponseItem {
    GetTeamEventsResponseItem {
        id: b.id,
        notes: b.notes.clone(),
        datetime: None,
        location_id: None,
        location: None,
        status: None,
        rescheduled_team_event_id: None,
        home_team_id: None,
        home_team_name: None,
        home_team_record: None,
        away_team_id: None,
        away_team_name: None,
        away_team_record: None,
        bye_date: Some(b.datetime.date()),
        bye_team_id: Some(b.team_id),
        bye_team_name: Some(team_names[&b.team_id].clone()),
        bye_team_record: Some(records.get(&b.team_id).map(|r| r.clone().into()).unwrap_or(Record::empty().into())),
        games: None
    }
}
