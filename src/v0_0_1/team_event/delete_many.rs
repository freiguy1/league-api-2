use rocket::response::content;
use std::collections::HashSet;

use error::Error;
use guards::{DbConn, LeagueAdmin, Cache};
use league_db::repo::TeamEventRepo;
use response::LeagueResponse;

type Response = Result<LeagueResponse<content::Json<&'static str>>, Error>;

#[delete("/leagues/<league_id>/team_events/all")]
pub fn handle(league_id: i32,
              conn: DbConn,
              cache: Cache,
              _user_id: LeagueAdmin) -> Response {
    let team_event_repo = TeamEventRepo::new(&conn);

    let existing_team_events = team_event_repo.get_by_league_ids(&vec![league_id])?;

    let team_ids: HashSet<i32> = existing_team_events.iter().flat_map(|g| g.team_ids()).collect();

    for team_id in team_ids.into_iter() {
        ::util::record::invalidate_records_for_team_id(team_id, cache.clone());
    }

    let team_event_ids: Vec<i32> = existing_team_events.iter().map(|g| g.id()).collect();

    team_event_repo.delete_many(&team_event_ids)?;

    let json = content::Json("{ \"success\": true }");
    Ok(LeagueResponse::success(json))
}
