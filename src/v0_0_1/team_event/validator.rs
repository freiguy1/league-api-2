use chrono::{DateTime, NaiveDate, Utc};
use std::collections::HashSet;
use league_db::repo::{TeamEvent as DbTeamEvent, Matchup};

fn validate_matchup_update<TE: TeamEvent>(te: &TE, existing_matchup: &Matchup, result: &mut Vec<String>) {
    let existing_game_ids: HashSet<i32> = existing_matchup.games.iter().map(|g| g.id).collect();
    let te_game_ids: HashSet<i32> = te.get_games().unwrap().iter().filter_map(|g| g.get_id()).collect();
    if !te_game_ids.is_subset(&existing_game_ids) {
        result.push("Can only update games which exist for matchup".to_string());
    }
}

pub fn validate<TE: TeamEvent>(te: &TE,
    statuses: &Vec<::util::KeyValue>,
    team_ids: &Vec<i32>,
    location_ids: &Vec<i32>,
    existing_team_event_ids: &Vec<i32>,
    existing_team_event: Option<&DbTeamEvent>) -> Vec<String> {
    let mut result = Vec::new();

    let games_opt = te.get_games();

    let is_matchup: bool = te.get_home_team_id().is_some() &&
        te.get_away_team_id().is_some() &&
        te.get_datetime().is_some() &&
        te.get_location_id().is_some() &&
        te.get_status().is_some() &&
        games_opt.is_some();
    let is_bye: bool = te.get_bye_date().is_some() && te.get_bye_team_id().is_some();
    if is_matchup == is_bye {
        result.push("Matchups require home_team_id, away_team_id, datetime, location_id, status, and games. Byes require bye_date and bye_team_id.".to_string());
    }

    if is_matchup {
        match existing_team_event {
            Some(&DbTeamEvent::Matchup(ref dbmu)) => validate_matchup_update(te, dbmu, &mut result),
            Some(&DbTeamEvent::Bye(_)) => result.push("A bye cannot be updated to a matchup".to_string()),
            _ => {}
        }

        let status_opt = statuses
            .iter()
            .filter(|s| s.value.to_lowercase() == te.get_status().unwrap().to_lowercase())
            .next();
        if status_opt.is_none() {
            result.push("status must contain a valid status choice".to_string());
        }

        if !team_ids.contains(&te.get_home_team_id().unwrap()) {
            result.push("home_team_id must exist for given league".to_string());
        }

        if !team_ids.contains(&te.get_away_team_id().unwrap()) {
            result.push("away_team_id must exist for given league".to_string());
        }

        if te.get_rescheduled_team_event_id().map_or(false, |id| !existing_team_event_ids.contains(&id)) {
            result.push("rescheduled_team_event_id must exist for given league".to_string());
        }

        if !location_ids.contains(&te.get_location_id().unwrap()) {
            result.push("location_id must exist for given league".to_string());
        }

        let games = games_opt.unwrap();
        let game_notes: Vec<&String> = games.iter()
            .filter_map(|g| g.get_notes()).collect();
        if game_notes.iter().any(|n| n.len() > 2000) {
            result.push("game notes must be less than 2000 characters".to_string());
        }

        for game in games.iter() {
            if game.get_home_team_score().is_some() != game.get_away_team_score().is_some() {
                result.push("game home team score and away team score must both be null or not null".to_string());
                break;
            }
        }

        let game_sequences: HashSet<i32> = games.iter()
            .map(|g| g.get_sequence()).collect();
        if game_sequences.len() != games.len() {
            result.push("game sequences cannot be repeated".to_string());
        }
    }

    if is_bye {
        match existing_team_event {
            Some(&DbTeamEvent::Matchup(_)) => result.push("A matchup cannot be updated to a bye".to_string()),
            //Some(&DbTeamEvent::Bye(_)) => result.push("A bye cannot be updated to a matchup".to_string()),
            _ => {}
        }

        if !team_ids.contains(&te.get_bye_team_id().unwrap()) {
            result.push("bye_team_id must exist for given league".to_string());
        }
    }

    if te.get_notes().as_ref().map_or(false, |n| n.len() > 2000) {
        result.push("notes must be between 0 and 2000 characters".to_string());
    }

    result
}

pub trait TeamEvent {
    // Shared
    fn get_notes(&self) -> Option<&String>;
    //Matchup
    fn get_datetime(&self) -> Option<&DateTime<Utc>>; // Required
    fn get_location_id(&self) -> Option<i32>; // Required
    fn get_status(&self) -> Option<&String>; // Required
    fn get_rescheduled_team_event_id(&self) -> Option<i32>;
    fn get_home_team_id(&self) -> Option<i32>; // Required
    fn get_away_team_id(&self) -> Option<i32>; // Required
    fn get_games(&self) -> Option<Vec<&Game>>; // Required
    //Bye
    fn get_bye_date(&self) -> Option<&NaiveDate>; // Required
    fn get_bye_team_id(&self) -> Option<i32>; // Required
}

pub trait Game {
    fn get_id(&self) -> Option<i32>;
    fn get_sequence(&self) -> i32;
    fn get_home_team_score(&self) -> Option<i32>;
    fn get_away_team_score(&self) -> Option<i32>;
    fn get_notes(&self) -> Option<&String>;
}

impl Game for super::contract::NewGame {
    fn get_id(&self) -> Option<i32> { None }
    fn get_sequence(&self) -> i32 { self.sequence }
    fn get_home_team_score(&self) -> Option<i32> { self.home_team_score }
    fn get_away_team_score(&self) -> Option<i32> { self.away_team_score }
    fn get_notes(&self) -> Option<&String> { self.notes.as_ref() }
}

impl TeamEvent for super::contract::CreateTeamEventRequest {
    // Shared
    fn get_notes(&self) -> Option<&String> { self.notes.as_ref() }
    //Matchup
    fn get_datetime(&self) -> Option<&DateTime<Utc>> { self.datetime.as_ref() }
    fn get_location_id(&self) -> Option<i32> { self.location_id }
    fn get_status(&self) -> Option<&String> { self.status.as_ref() }
    fn get_rescheduled_team_event_id(&self) -> Option<i32> { self.rescheduled_team_event_id }
    fn get_home_team_id(&self) -> Option<i32> { self.home_team_id }
    fn get_away_team_id(&self) -> Option<i32> { self.away_team_id }
    fn get_games(&self) -> Option<Vec<&Game>> {
        self.games.as_ref().map(|s| s.iter().map(|g| g as &Game).collect())
    }
    //Bye
    fn get_bye_date(&self) -> Option<&NaiveDate> { self.bye_date.as_ref() }
    fn get_bye_team_id(&self) -> Option<i32> { self.bye_team_id }
}

impl TeamEvent for super::contract::CreateTeamEventsRequestItem {
    // Shared
    fn get_notes(&self) -> Option<&String> { self.notes.as_ref() }
    //Matchup
    fn get_datetime(&self) -> Option<&DateTime<Utc>> { self.datetime.as_ref() }
    fn get_location_id(&self) -> Option<i32> { self.location_id }
    fn get_status(&self) -> Option<&String> { self.status.as_ref() }
    fn get_rescheduled_team_event_id(&self) -> Option<i32> { None }
    fn get_home_team_id(&self) -> Option<i32> { self.home_team_id }
    fn get_away_team_id(&self) -> Option<i32> { self.away_team_id }
    fn get_games(&self) -> Option<Vec<&Game>> {
        self.games.as_ref().map(|s| s.iter().map(|g| g as &Game).collect())
    }
    //Bye
    fn get_bye_date(&self) -> Option<&NaiveDate> { self.bye_date.as_ref() }
    fn get_bye_team_id(&self) -> Option<i32> { self.bye_team_id }
}

impl Game for super::contract::UpdateGame {
    fn get_id(&self) -> Option<i32> { self.id }
    fn get_sequence(&self) -> i32 { self.sequence }
    fn get_home_team_score(&self) -> Option<i32> { self.home_team_score }
    fn get_away_team_score(&self) -> Option<i32> { self.away_team_score }
    fn get_notes(&self) -> Option<&String> { self.notes.as_ref() }
}

impl TeamEvent for super::contract::UpdateTeamEventRequest {
    // Shared
    fn get_notes(&self) -> Option<&String> { self.notes.as_ref() }
    //Matchup
    fn get_datetime(&self) -> Option<&DateTime<Utc>> { self.datetime.as_ref() }
    fn get_location_id(&self) -> Option<i32> { self.location_id }
    fn get_status(&self) -> Option<&String> { self.status.as_ref() }
    fn get_rescheduled_team_event_id(&self) -> Option<i32> { self.rescheduled_team_event_id }
    fn get_home_team_id(&self) -> Option<i32> { self.home_team_id }
    fn get_away_team_id(&self) -> Option<i32> { self.away_team_id }
    fn get_games(&self) -> Option<Vec<&Game>> {
        self.games.as_ref().map(|s| s.iter().map(|g| g as &Game).collect())
    }
    //Bye
    fn get_bye_date(&self) -> Option<&NaiveDate> { self.bye_date.as_ref() }
    fn get_bye_team_id(&self) -> Option<i32> { self.bye_team_id }
}
