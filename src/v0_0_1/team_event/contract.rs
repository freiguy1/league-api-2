use chrono::{DateTime, Utc, NaiveDate};

#[derive(Debug, Clone, Serialize)]
pub struct GetTeamEventsResponseItem {
    // Shared
    pub id: i32,
    pub notes: Option<String>,
    //Matchup
    pub datetime: Option<DateTime<Utc>>,
    pub location_id: Option<i32>,
    pub location: Option<String>,
    pub status: Option<String>,
    pub rescheduled_team_event_id: Option<i32>,
    pub home_team_id: Option<i32>,
    pub home_team_record: Option<Record>,
    pub home_team_name: Option<String>,
    pub away_team_id: Option<i32>,
    pub away_team_record: Option<Record>,
    pub away_team_name: Option<String>,
    pub games: Option<Vec<Game>>,
    //Bye
    pub bye_date: Option<NaiveDate>,
    pub bye_team_id: Option<i32>,
    pub bye_team_record: Option<Record>,
    pub bye_team_name: Option<String>
}

#[derive(Debug, Clone, Serialize)]
pub struct Record {
    pub wins: i32,
    pub losses: i32,
    pub ties: i32
}

impl From<::stats::record::Record> for Record {
    fn from(record: ::stats::record::Record) -> Self {
        Record {
            wins: record.win,
            losses: record.loss,
            ties: record.tie
        }
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct Game {
    pub id: i32,
    pub sequence: i32,
    pub home_team_score: Option<i32>,
    pub away_team_score: Option<i32>,
    pub notes: Option<String>
}

#[derive(Debug, Clone, Deserialize)]
pub struct CreateTeamEventRequest {
    // Shared
    pub notes: Option<String>,
    //Matchup
    pub datetime: Option<DateTime<Utc>>, // Required
    pub location_id: Option<i32>, // Required
    pub status: Option<String>, // Required
    pub rescheduled_team_event_id: Option<i32>,
    pub home_team_id: Option<i32>, // Required
    pub away_team_id: Option<i32>, // Required
    pub games: Option<Vec<NewGame>>, // Required
    //Bye
    pub bye_date: Option<NaiveDate>, // Required
    pub bye_team_id: Option<i32>, // Required
}

#[derive(Debug, Clone, Serialize)]
pub struct CreateTeamEventResponse {
    pub id: i32
}

#[derive(Debug, Clone, Deserialize)]
pub struct CreateTeamEventsRequestItem {
    // Shared
    pub notes: Option<String>,
    //Matchup
    pub datetime: Option<DateTime<Utc>>, // Required
    pub location_id: Option<i32>, // Required
    pub status: Option<String>, // Required
    pub home_team_id: Option<i32>, // Required
    pub away_team_id: Option<i32>, // Required
    pub games: Option<Vec<NewGame>>, // Required
    //Bye
    pub bye_date: Option<NaiveDate>, // Required
    pub bye_team_id: Option<i32>, // Required
}

#[derive(Debug, Clone, Deserialize)]
pub struct NewGame {
    pub sequence: i32,
    pub home_team_score: Option<i32>,
    pub away_team_score: Option<i32>,
    pub notes: Option<String>
}

#[derive(Debug, Clone, Serialize)]
pub struct GetTeamEventResponse {
    // Shared
    pub id: i32,
    pub notes: Option<String>,
    //Matchup
    pub datetime: Option<DateTime<Utc>>,
    pub location_id: Option<i32>,
    pub status: Option<String>,
    pub rescheduled_team_event_id: Option<i32>,
    pub home_team_id: Option<i32>,
    pub away_team_id: Option<i32>,
    pub games: Option<Vec<Game>>,
    //Bye
    pub bye_date: Option<NaiveDate>,
    pub bye_team_id: Option<i32>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct UpdateTeamEventRequest {
    // Shared
    pub notes: Option<String>,
    //Matchup
    pub datetime: Option<DateTime<Utc>>, // Required
    pub location_id: Option<i32>, // Required
    pub status: Option<String>, // Required
    pub rescheduled_team_event_id: Option<i32>,
    pub home_team_id: Option<i32>, // Required
    pub away_team_id: Option<i32>, // Required
    pub games: Option<Vec<UpdateGame>>, // Required
    //Bye
    pub bye_date: Option<NaiveDate>, // Required
    pub bye_team_id: Option<i32>, // Required
}

#[derive(Debug, Clone, Deserialize)]
pub struct UpdateGame {
    pub id: Option<i32>,
    pub sequence: i32,
    pub home_team_score: Option<i32>,
    pub away_team_score: Option<i32>,
    pub notes: Option<String>
}
