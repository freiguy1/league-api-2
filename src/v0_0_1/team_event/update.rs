use chrono::NaiveTime;
use rocket_contrib::Json;
use rocket::response::content;
use std::collections::HashSet;

use super::contract::UpdateTeamEventRequest;
use error::Error;
use guards::{DbConn, LeagueAdminForTeamEvent, Cache};
use league_db::repo::{NewGame, TeamEventRepo, TeamEvent,
    Game, LeagueRepo, TeamRepo, UpdateMatchup, Bye};
use response::LeagueResponse;

type Response = Result<LeagueResponse<content::Json<&'static str>>, Error>;

#[put("/team_events/<team_event_id>", data = "<update_team_event_request>")]
pub fn handle(team_event_id: i32,
              update_team_event_request: Json<UpdateTeamEventRequest>,
              conn: DbConn,
              cache: Cache,
              _user_id: LeagueAdminForTeamEvent) -> Response {
    let update_team_event_request = update_team_event_request.into_inner();
    let team_event_repo = TeamEventRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);
    let reference = ::util::Reference::new(cache.clone(), &conn);
    let statuses = reference.matchup_statuses();

    let existing_team_event = team_event_repo.get_single(team_event_id)?
        .expect("Guard guarantees team event exists");
    let league_id = existing_team_event.league_id();
    let existing_team_events = team_event_repo.get_by_league_ids(&vec![league_id])?;
    let league = league_repo.get_single(league_id)?
        .expect("Guard guarantees league exists");
    let teams = team_repo.get_by_league_id(league_id)?;

    let validation_errors = super::validator::validate(
        &update_team_event_request,
        &statuses,
        &teams.iter().map(|t| t.id).collect(),
        &league.matchup_locations.iter().map(|l| l.id).collect(),
        &existing_team_events.iter().map(|g| g.id()).collect(),
        Some(&existing_team_event));

    if !validation_errors.is_empty() {
        return Ok(LeagueResponse::bad_request(validation_errors))
    }

    if update_team_event_request.home_team_id.is_some() { // Matchup 
        let update_dao = build_matchup_dao(&update_team_event_request,
                                           league_id,
                                           team_event_id,
                                           &statuses,
                                           existing_team_event);
        ::util::record::invalidate_records_for_team_id(update_dao.home_team_id, cache.clone());
        ::util::record::invalidate_records_for_team_id(update_dao.away_team_id, cache.clone());
        team_event_repo.update_matchup(&update_dao)?;
    } else { // bye
        let update_dao = build_bye_dao(&update_team_event_request, league_id, team_event_id);
        ::util::record::invalidate_records_for_team_id(update_dao.team_id, cache.clone());
        team_event_repo.update_bye(&update_dao)?;
    }

    let json = content::Json("{ \"success\": true }");
    Ok(LeagueResponse::success(json))
}

fn build_matchup_dao(req: &UpdateTeamEventRequest,
                     league_id: i32,
                     team_event_id: i32,
                     statuses: &Vec<::util::KeyValue>,
                     existing_team_event: TeamEvent) -> UpdateMatchup {
    let req_games = req.games.as_ref().unwrap();
    let new_games = req_games.iter()
        .filter(|g| g.id.is_none())
        .map(|g| NewGame {
            home_team_score: g.home_team_score,
            away_team_score: g.away_team_score,
            sequence: g.sequence,
            notes: g.notes.clone()
        }).collect();
    let update_games = req_games.iter()
        .filter(|g| g.id.is_some())
        .map(|g| Game {
            id: g.id.unwrap(),
            home_team_score: g.home_team_score,
            away_team_score: g.away_team_score,
            sequence: g.sequence,
            notes: g.notes.clone()
        }).collect();
    let keep_games: HashSet<i32> = req_games.iter().filter_map(|g| g.id).collect();
    let existing_games: HashSet<i32> = existing_team_event.matchup_ref().unwrap()
        .games.iter().map(|g| g.id).collect();
    let delete_games = existing_games.difference(&keep_games).map(|id| *id).collect();

    UpdateMatchup {
        id: team_event_id,
        datetime: req.datetime.unwrap().naive_utc(),
        matchup_location_id: req.location_id.unwrap(),
        league_id: league_id,
        home_team_id: req.home_team_id.unwrap(),
        away_team_id: req.away_team_id.unwrap(),
        matchup_status_id: statuses.iter().filter(|s| s.value.to_lowercase() == req.status.as_ref().unwrap().to_lowercase()).next().unwrap().key,
        rescheduled_team_event_id: req.rescheduled_team_event_id,
        notes: req.notes.clone(),
        new_games: new_games,
        update_games: update_games,
        delete_games: delete_games
    }
}

fn build_bye_dao(req: &UpdateTeamEventRequest,
                 league_id: i32,
                 team_event_id: i32) -> Bye {
    Bye {
        id: team_event_id,
        datetime: req.bye_date.unwrap().and_time(NaiveTime::from_hms(0, 0, 0)),
        league_id: league_id,
        team_id: req.bye_team_id.unwrap(),
        notes: req.notes.clone()
    }
}
