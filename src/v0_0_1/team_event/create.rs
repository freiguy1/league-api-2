use chrono::NaiveTime;
use rocket_contrib::Json;

use super::contract::{CreateTeamEventRequest, CreateTeamEventResponse};
use error::Error;
use guards::{DbConn, LeagueAdmin, Cache};
use league_db::repo::{NewMatchup, NewBye, NewGame, TeamEventRepo, LeagueRepo, TeamRepo};
use response::LeagueResponse;

type Response = Result<LeagueResponse<Json<CreateTeamEventResponse>>, Error>;

#[post("/leagues/<league_id>/team_events", data = "<create_team_event_request>")]
pub fn handle(league_id: i32,
              create_team_event_request: Json<CreateTeamEventRequest>,
              conn: DbConn,
              cache: Cache,
              _user_id: LeagueAdmin) -> Response {
    let create_team_event_request = create_team_event_request.into_inner();
    let team_event_repo = TeamEventRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);
    let reference = ::util::Reference::new(cache.clone(), &conn);
    let statuses = reference.matchup_statuses();

    let league = league_repo.get_single(league_id)?.expect("Guard guarantees league exists");

    let teams = team_repo.get_by_league_id(league_id)?;
    let existing_team_events = team_event_repo.get_by_league_ids(&vec![league_id])?;
    let validation_errors = super::validator::validate(
        &create_team_event_request,
        &statuses,
        &teams.iter().map(|t| t.id).collect(),
        &league.matchup_locations.iter().map(|l| l.id).collect(),
        &existing_team_events.iter().map(|g| g.id()).collect(),
        None);

    if !validation_errors.is_empty() {
        return Ok(LeagueResponse::bad_request(validation_errors));
    }

    let new_team_event_id = if create_team_event_request.home_team_id.is_some() {
        let dao = build_new_matchup_dao(league_id, create_team_event_request, &statuses);

        ::util::record::invalidate_records_for_team_id(dao.home_team_id, cache.clone());
        ::util::record::invalidate_records_for_team_id(dao.away_team_id, cache.clone());

        let dao = team_event_repo.create_matchup(&dao)?;

        dao.id()
    } else {
        let dao = build_new_bye_dao(league_id, create_team_event_request);

        ::util::record::invalidate_records_for_team_id(dao.team_id, cache.clone());

        let dao = team_event_repo.create_bye(&dao)?;
        dao.id()
    };

    let create_team_event_response = CreateTeamEventResponse {
        id: new_team_event_id
    };
    Ok(LeagueResponse::success(Json(create_team_event_response)))
}

fn build_new_matchup_dao(league_id: i32, req: CreateTeamEventRequest, statuses: &Vec<::util::KeyValue>) -> NewMatchup {
    let games: Vec<NewGame> = if let Some(ref games) = req.games { 
        games.iter().map(|g| NewGame {
            sequence: g.sequence,
            home_team_score: g.home_team_score,
            away_team_score: g.away_team_score,
            notes: g.notes.clone()
        }).collect()
    } else {
        Vec::new()
    };

    NewMatchup {
        datetime: req.datetime.unwrap().naive_utc(),
        matchup_location_id: req.location_id.unwrap(),
        league_id: league_id,
        home_team_id: req.home_team_id.unwrap(),
        away_team_id: req.away_team_id.unwrap(),
        matchup_status_id: statuses.iter().filter(|s| s.value.to_lowercase() == req.status.as_ref().unwrap().to_lowercase()).next().unwrap().key,
        rescheduled_team_event_id: req.rescheduled_team_event_id,
        notes: req.notes.clone(),
        games: games
    }
}

fn build_new_bye_dao(league_id: i32, req: CreateTeamEventRequest) -> NewBye {
    NewBye {
        datetime: req.bye_date.unwrap().and_time(NaiveTime::from_hms(0, 0, 0)),
        league_id: league_id,
        team_id: req.bye_team_id.unwrap(),
        notes: req.notes.clone()
    }
}

