use rocket_contrib::Json;
use std::collections::HashSet;
use std::ops::DerefMut;

use error::Error;
use guards::{DbConn, LeagueAdmin};
use response::LeagueResponse;
use super::contract::{CreateTeamRequest, CreateTeamResponse};
use league_db::repo::{NewTeam, TeamRepo, LeagueRepo, TeamDao};

type Response = Result<LeagueResponse<Json<CreateTeamResponse>>, Error>;

#[post("/<league_id>/teams", data = "<create_team_request>")]
pub fn handle(league_id: i32, mut create_team_request: Json<CreateTeamRequest>, conn: DbConn, _user_id: LeagueAdmin) -> Response {
    let validation_errors = super::validator::validate(create_team_request.deref_mut());
    if !validation_errors.is_empty() {
        return Ok(LeagueResponse::bad_request(validation_errors));
    }

    let team_repo = TeamRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);

    if league_repo.get_single(league_id)?.is_none() {
        return Ok(LeagueResponse::not_found());
    }

    let existing_teams = team_repo.get_by_league_id(league_id)?;

    let team_sequence = get_team_sequence(&existing_teams);

    let dao = build_dao(league_id, team_sequence, create_team_request.into_inner());

    let created_dao = team_repo.create(&dao)?;

    let response = CreateTeamResponse {
        sequence: team_sequence,
        id: created_dao.id
    };

    Ok(LeagueResponse::success(Json(response)))
}

fn get_team_sequence(existing_teams: &Vec<TeamDao>) -> i32 {
    if existing_teams.len() == 0 {
        return 1;
    }
    let mut team_sequence: Vec<i32> = existing_teams.iter().map(|team| team.sequence).collect();
    team_sequence.sort();
    if team_sequence.len() == team_sequence[team_sequence.len() - 1] as usize {
        (team_sequence.len() + 1) as i32
    } else {
        let team_sequence: HashSet<i32> = team_sequence.iter().map(|&id| id).collect();
        let range_set: HashSet<i32> = (1..(team_sequence.len() + 1)).map(|num| num as i32).collect();
        *range_set.difference(&team_sequence).next().unwrap()
    }
}

fn build_dao(league_id: i32, team_sequence: i32, req: CreateTeamRequest) -> NewTeam {
    NewTeam {
        sequence: team_sequence,
        league_id: league_id,
        name: req.name,
        captain_name: req.captain_name,
        captain_email: req.captain_email,
        captain_phone: req.captain_phone
    }
}


