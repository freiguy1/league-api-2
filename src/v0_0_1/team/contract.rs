use chrono::{DateTime, Utc, NaiveDate};

#[derive(Debug, Clone, Deserialize)]
pub struct CreateTeamRequest {
    pub name: String,
    pub captain_name: String,
    pub captain_email: String,
    pub captain_phone: String
}

#[derive(Debug, Clone, Deserialize)]
pub struct UpdateTeamRequest {
    pub name: String,
    pub captain_name: String,
    pub captain_email: String,
    pub captain_phone: String
}

#[derive(Debug, Clone, Serialize)]
pub struct CreateTeamResponse {
    pub sequence: i32,
    pub id: i32
}

#[derive(Debug, Clone, Serialize)]
pub struct GetTeamsResponseItem {
    pub sequence: i32,
    pub id: i32,
    pub name: String,
    pub captain_name: Option<String>,
    pub captain_email: Option<String>,
    pub captain_phone: Option<String>,
    pub wins: i32,
    pub losses: i32,
    pub ties: i32,
    pub is_followed: bool
}

#[derive(Serialize)]
pub struct GetTeamResponse {
    pub sequence: i32,
    pub id: i32,
    pub name: String,
    pub league_id: i32,
    pub league_name: String,
    pub league_organization: String,
    pub captain_name: Option<String>,
    pub captain_email: Option<String>,
    pub captain_phone: Option<String>,
    pub wins: i32,
    pub losses: i32,
    pub ties: i32,
    pub remaining_team_events: i32,
    pub sport: String,
    pub next_team_events: Vec<TeamEventSummary>,
    pub previous_team_event: Option<TeamEventSummary>,
    pub is_followed: bool,
    pub num_followers: i32
}

#[derive(Serialize)]
pub struct TeamEventSummary {
    // Shared
    pub id: i32,
    pub notes: Option<String>,
    // Matchup
    pub datetime: Option<DateTime<Utc>>,
    pub location: Option<String>,
    pub is_home: Option<bool>,
    pub against_team_name: Option<String>,
    pub status: Option<String>,
    pub games: Option<Vec<Game>>,
    //Bye
    pub bye_date: Option<NaiveDate>
}

#[derive(Serialize)]
pub struct Game {
    pub id: i32,
    pub home_team_score: Option<i32>,
    pub away_team_score: Option<i32>,
    pub sequence: i32,
    pub notes: Option<String>
}