use rocket::response::content;
use rocket_contrib::Json;
use std::ops::DerefMut;

use error::Error;
use guards::{DbConn, LeagueAdminForTeam};
use response::LeagueResponse;
use super::contract::UpdateTeamRequest;
use league_db::repo::{TeamRepo, TeamDao};

type Response = Result<LeagueResponse<content::Json<&'static str>>, Error>;

#[put("/<team_id>", data = "<update_team_request>")]
pub fn handle(team_id: i32, mut update_team_request: Json<UpdateTeamRequest>, conn: DbConn, _user_id: LeagueAdminForTeam) -> Response {
    let validation_errors = super::validator::validate(update_team_request.deref_mut());
    if !validation_errors.is_empty() {
        return Ok(LeagueResponse::bad_request(validation_errors));
    }

    let team_repo = TeamRepo::new(&conn);

    let existing_team = team_repo.get_by_id(team_id)?.expect("Team should exist since LeagueAdminForTeam guard in place");

    let dao = build_dao(existing_team, update_team_request.into_inner());

    team_repo.update(&dao)?;

    let json = content::Json("{ \"success\": true }");
    Ok(LeagueResponse::success(json))
}

fn build_dao(existing_team: TeamDao, req: UpdateTeamRequest) -> TeamDao {
    TeamDao {
        id: existing_team.id,
        sequence: existing_team.sequence,
        league_id: existing_team.league_id,
        name: req.name,
        captain_name: req.captain_name,
        captain_email: req.captain_email,
        captain_phone: req.captain_phone
    }
}
