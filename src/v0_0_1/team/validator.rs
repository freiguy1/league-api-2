pub fn validate<T: Team>(t: &mut T) -> Vec<String> {
    let mut result = Vec::new();
    if t.get_name().len() > 45 || t.get_name().len() < 5 {
        result.push("name must be between 5 and 45 characters".to_string());
    }
    if t.get_captain_name().len() > 45 || t.get_captain_name().len() < 5 {
        result.push("name must be between 5 and 45 characters".to_string());
    }

    if let Err(msg) = ::util::validate::is_valid_email(&mut t.get_mut_captain_email()) {
        result.push(msg.to_string());
    }

    if let Err(msg) = ::util::validate::is_valid_phone(&mut t.get_mut_captain_phone()) {
        result.push(msg.to_string());
    }
    result
}

pub trait Team {
    fn get_name(&self) -> &String;
    fn get_captain_name(&self) -> &String;
    fn get_mut_captain_email(&mut self) ->  &mut String;
    fn get_mut_captain_phone(&mut self) -> &mut String;
}

impl Team for super::contract::CreateTeamRequest {
    fn get_name(&self) -> &String { &self.name }
    fn get_captain_name(&self) -> &String { &self.captain_name }
    fn get_mut_captain_email(&mut self) ->  &mut String { &mut self.captain_email }
    fn get_mut_captain_phone(&mut self) -> &mut String { &mut self.captain_phone }
}

impl Team for super::contract::UpdateTeamRequest {
    fn get_name(&self) -> &String { &self.name }
    fn get_captain_name(&self) -> &String { &self.captain_name }
    fn get_mut_captain_email(&mut self) ->  &mut String { &mut self.captain_email }
    fn get_mut_captain_phone(&mut self) -> &mut String { &mut self.captain_phone }
}

#[cfg(test)]
mod tests {
    use super::validate;
    use super::super::contract::CreateTeamRequest;

    fn legit_request() -> CreateTeamRequest {
        CreateTeamRequest {
            name: "Team name".to_string(),
            captain_name: "Captain Name".to_string(),
            captain_email: "bar@tar.zar".to_string(),
            captain_phone: "1234567890".to_string(),
        }
    }

    #[test]
    fn test_validate_legit() {
        let mut request = legit_request();
        let validation_result = validate(&mut request);
        assert_eq!(0, validation_result.len());
    }

    #[test]
    fn test_validate_name_short() {
        let mut request = legit_request();
        request.name = "1234".to_string();
        let validation_result = validate(&mut request);
        assert_eq!(1, validation_result.len());
    }

    #[test]
    fn test_validate_name_long() {
        let mut request = legit_request();
        request.name = ::std::iter::repeat('m').take(46).collect::<String>();
        let validation_result = validate(&mut request);
        assert_eq!(1, validation_result.len());
    }

    // #[test]
    // fn test_validate_legit() {
        // let mut request = legit_request();
        // let validation_result = validate(&mut request);
        // assert_eq!(0, validation_result.len());
    // }

    #[test]
    fn test_validate_captain_name_short() {
        let mut request = legit_request();
        request.captain_name = "1234".to_string();
        let validation_result = validate(&mut request);
        assert_eq!(1, validation_result.len());
    }

    #[test]
    fn test_validate_captain_name_long() {
        let mut request = legit_request();
        request.captain_name = ::std::iter::repeat('m').take(46).collect::<String>();
        let validation_result = validate(&mut request);
        assert_eq!(1, validation_result.len());
    }

    // #[test]
    // fn test_validate_name_legit() {
        // let mut request = legit_request();
        // let validation_result = validate(&mut request);
        // assert_eq!(0, validation_result.len());
    // }
}
