use rocket::Rocket;

mod contract;
mod get_by_id;
mod get_all;
mod create;
mod delete;
mod update;
mod validator;

pub fn init_routes(rocket: Rocket) -> Rocket {
    let routes = routes![
        get_by_id::handle,
        delete::handle,
        update::handle
    ];
    let rocket = rocket.mount(&format!("/{}/teams/", super::VER), routes);

    let routes = routes![
        get_all::handle,
        create::handle,
    ];
    rocket.mount(&format!("/{}/leagues/", super::VER), routes)
}