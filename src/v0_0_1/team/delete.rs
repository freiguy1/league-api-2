use rocket::response::content;

use error::Error;
use guards::{DbConn, LeagueAdminForTeam};
use league_db::repo::{TeamEventRepo, TeamFollowerRepo, TeamRepo};
use response::LeagueResponse;

type Response = Result<LeagueResponse<content::Json<&'static str>>, Error>;

#[delete("/<team_id>")]
pub fn handle(team_id: i32, conn: DbConn, _user_id: LeagueAdminForTeam) -> Response {
    // A team does exist with this id because the LeagueAdminForTeam guard ran

    let team_repo = TeamRepo::new(&conn);
    let team_event_repo = TeamEventRepo::new(&conn);
    let follower_repo = TeamFollowerRepo::new(&conn);

    let team_events = team_event_repo.get_by_team_ids(&vec![team_id].into_iter().collect())?;

    if team_events.len() != 0 {
        return Ok(LeagueResponse::bad_request(vec!["Games or byes exist which reference this team"]));
    }

    follower_repo.delete_by_team_id(team_id)?;

    team_repo.delete(team_id)?;

    let json = content::Json("{ \"success\": true }");
    Ok(LeagueResponse::success(json))
}
