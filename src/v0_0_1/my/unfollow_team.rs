use error::Error;
use guards::{DbConn, UserRequired};
use league_db::repo::{TeamFollowerRepo, TeamRepo};
use response::LeagueResponse;

type Response = Result<LeagueResponse<()>, Error>;

#[delete("/teams/<team_id>")]
pub fn handle(team_id: i32, conn: DbConn, user_id: UserRequired) -> Response {

    let user_id = user_id.0;
    let follower_repo = TeamFollowerRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);

    if team_repo.get_by_id(team_id)?.is_none() {
        return Ok(LeagueResponse::not_found());
    }

    let mut followed_team_ids = follower_repo.get_teams_by_user_id(user_id)?
        .into_iter()
        .map(|(t, _)| t.id);

    if !followed_team_ids.any(|tid| tid == team_id) {
        return Ok(LeagueResponse::bad_request(vec!["team not yet followed"]));
    }

    follower_repo.delete(user_id, team_id)?;
    Ok(LeagueResponse::success(()))
}
