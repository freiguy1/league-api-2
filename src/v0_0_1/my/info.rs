use chrono::{DateTime, Utc};
use rocket_contrib::Json;
use std::collections::{HashSet, HashMap};

use super::contract::{InfoResponse, FollowedTeam, NextTeamEvent, League, MatchupLocation};
use guards::{DbConn, Cache, UserRequired};
use league_db::repo::{TeamFollowerRepo, TeamDao, TeamEventRepo, LeagueRepo, LeagueDao, TeamEvent, TeamRepo, UserRepo};
use response::LeagueResponse;
use error::Error;
use stats::record::Record;
use util::KeyValue;

type Response = Result<LeagueResponse<Json<InfoResponse>>, Error>;

#[get("/info")]
pub fn handle(conn: DbConn, cache: Cache, user_id: UserRequired) -> Response {
    let teams = get_teams(user_id.0, &conn, &cache)?;
    let leagues = get_leagues(user_id.0, &conn, &cache)?;

    let user_repo = UserRepo::new(&conn);
    let user_dao = user_repo.get_single(user_id.0)?.expect("UserRequired guard guarantees one exists");

    let response = InfoResponse {
        user_id: user_dao.id,
        first_name: user_dao.first_name,
        last_name: user_dao.last_name,
        email: user_dao.email,
        teams: teams,
        leagues: leagues
    };
    Ok(LeagueResponse::success(Json(response)))
}

fn get_leagues(user_id: i32, conn: &DbConn, cache: &Cache) -> Result<Vec<League>, Error> {
    let league_repo = LeagueRepo::new(&conn);
    let reference = ::util::Reference::new(cache.0.clone(), &conn);
    let sports = reference.sports();
    let team_repo = TeamRepo::new(&conn);

    let league_ids: HashSet<i32> = league_repo
        .get_leagues_by_administrator_user_id(user_id)?
        .iter().map(|id| *id).collect();
    let leagues = league_repo.get_many(&league_ids)?;
    let teams = team_repo.get_count_by_league_ids(&league_ids)?;

    let result: Vec<League> = leagues
        .into_iter()
        .map(|l| build_league_response(l, &teams, &sports))
        .collect();

    Ok(result)
}

fn build_league_response(dao: LeagueDao, team_counts: &HashMap<i32, i32>, sports: &Vec<KeyValue>) -> League {
    let sport = sports.iter().find(|s| s.key == dao.sport_id).unwrap().value.clone();
    League {
        id: dao.id,
        name: dao.name,
        information: dao.information,
        sport: sport,
        matchup_locations: dao.matchup_locations.into_iter().map(|loc| MatchupLocation {
            id: loc.id,
            name: loc.name
        }).collect(),
        organization: dao.organization,
        contact_phone: dao.contact_phone,
        team_count: team_counts[&dao.id],
        games_per_matchup: dao.games_per_matchup
    }
}

fn get_teams(user_id: i32, conn: &DbConn, cache: &Cache) -> Result<Vec<FollowedTeam>, Error> {
    let follower_repo = TeamFollowerRepo::new(&conn);
    let team_event_repo = TeamEventRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);
    let reference = ::util::Reference::new(cache.0.clone(), &conn);

    let sports = reference.sports();
    let teams: Vec<TeamDao> = follower_repo.get_teams_by_user_id(user_id)?
        .into_iter().map(|(t, _)| t).collect();

    let team_ids: HashSet<i32> = teams.iter().map(|t| t.id).collect();
    let league_ids: HashSet<i32> = teams.iter().map(|t| t.league_id).collect();

    let league_daos: HashMap<i32, LeagueDao> = league_repo.get_many(&league_ids)?
        .into_iter().map(|dao| (dao.id, dao)).collect();
    let mut team_events: Vec<TeamEvent> = team_event_repo.get_by_team_ids(&team_ids)?
        .into_iter().filter(|t| t.datetime() > &Utc::now().naive_utc()).collect();
    team_events.sort_by_key(|t| t.datetime().clone());
    let records = ::util::record::get_records_for_team_ids(team_ids, cache.0.clone(), &team_event_repo)?;
    let team_names: HashMap<i32, String> = team_repo.get_by_league_ids(&league_ids)?
        .into_iter().map(|t| (t.id, t.name)).collect();
    Ok(build_teams_response(teams, records, league_daos, team_events, sports, team_names))
}

fn build_teams_response(daos: Vec<TeamDao>,
                  records: HashMap<i32, Record>,
                  leagues: HashMap<i32, LeagueDao>,
                  team_events: Vec<TeamEvent>,
                  sports: Vec<::util::KeyValue>,
                  team_names: HashMap<i32, String>) -> Vec<FollowedTeam> {
    let default_record = Record { win: 0, loss: 0, tie: 0 };
    daos
        .into_iter()
        .map(|team| {
            let ref league = leagues[&team.league_id];
            let record = records.get(&team.id).unwrap_or(&default_record);
            let team_events = team_events.iter().filter(|t| t.team_ids().iter().any(|tid| tid == &team.id)).collect::<Vec<_>>();
            let next_team_events = if let Some(next_team_event) = team_events.first() {
                let date = next_team_event.datetime().date();
                team_events.iter().filter(|te| te.datetime().date() == date)
                    .map(|te| make_next_team_event(te, team.id, &league, &team_names))
                    .collect()
            } else { vec![] };
            let sport = sports.iter().filter(|s| s.key == league.sport_id).next().unwrap().value.clone();

            FollowedTeam {
                sequence: team.sequence,
                id: team.id,
                name: team.name.clone(),
                league_id: team.league_id,
                league_name: league.name.clone(),
                sport: sport,
                wins: record.win,
                losses: record.loss,
                ties: record.tie,
                remaining_team_events: team_events.len() as i32,
                next_team_events: next_team_events
            }
        })
        .collect()
}

fn make_next_team_event(te: &TeamEvent,
                  team_id: i32,
                  league: &LeagueDao,
                  team_name: &HashMap<i32, String>) -> NextTeamEvent {
    match te {
        &TeamEvent::Matchup(ref m) => {
            NextTeamEvent {
                id: m.id,
                notes: m.notes.clone(),
                datetime: Some(DateTime::from_utc(m.datetime, Utc)),
                location: Some(league.matchup_locations.iter().find(|gl| gl.id == m.matchup_location_id).unwrap().name.clone()),
                is_home: Some(team_id == m.home_team_id),
                against_team_name: Some(
                    if team_id == m.home_team_id { team_name[&m.away_team_id].clone() }
                    else { team_name[&m.home_team_id].clone() }),
                bye_date: None
            }
        },
        &TeamEvent::Bye(ref b) => NextTeamEvent {
            id: b.id,
            notes: b.notes.clone(),
            datetime: None,
            location: None,
            is_home: None,
            against_team_name: None,
            bye_date: Some(b.datetime.date())
        }
    }
}