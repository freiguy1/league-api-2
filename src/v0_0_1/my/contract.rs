use chrono::{NaiveDate, Utc, DateTime};

#[derive(Debug, Clone, Serialize)]
pub struct InfoResponse {
    pub user_id: i32,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub teams: Vec<FollowedTeam>,
    pub leagues: Vec<League>
}

#[derive(Debug, Clone, Serialize)]
pub struct FollowedTeam {
    pub sequence: i32,
    pub id: i32,
    pub league_name: String,
    pub league_id: i32,
    pub name: String,
    pub wins: i32,
    pub losses: i32,
    pub ties: i32,
    pub remaining_team_events: i32,
    pub sport: String,
    pub next_team_events: Vec<NextTeamEvent>
}

#[derive(Debug, Clone, Serialize)]
pub struct NextTeamEvent {
    // Shared
    pub id: i32,
    pub notes: Option<String>,
    // Matchup
    pub datetime: Option<DateTime<Utc>>,
    pub location: Option<String>,
    pub is_home: Option<bool>,
    pub against_team_name: Option<String>,
    // Bye
    pub bye_date: Option<NaiveDate>
}

#[derive(Debug, Clone, Serialize)]
pub struct League {
    pub id: i32,
    pub name: String,
    pub information: Option<String>,
    pub sport: String,
    pub matchup_locations: Vec<MatchupLocation>,
    pub organization: String,
    pub contact_phone: String,
    pub team_count: i32,
    pub games_per_matchup: i32
}

#[derive(Debug, Clone, Serialize)]
pub struct MatchupLocation {
    pub id: i32,
    pub name: String
}
