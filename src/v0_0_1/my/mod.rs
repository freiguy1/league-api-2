use rocket::Rocket;

mod contract;
mod follow_team;
mod unfollow_team;
mod info;

pub fn init_routes(rocket: Rocket) -> Rocket {
    let routes = routes![
        follow_team::handle,
        unfollow_team::handle,
        info::handle
    ];
    rocket.mount(&format!("/{}/my/", super::VER), routes)
}
