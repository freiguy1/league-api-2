#[derive(Debug, Clone, Deserialize)]
pub struct CreateLeagueRequest {
    pub name: String,
    pub information: Option<String>,
    pub sport: String,
    pub matchup_locations: Vec<String>,
    pub organization: String,
    pub contact_phone: String,
    pub games_per_matchup: u8
}

#[derive(Debug, Clone, Serialize)]
pub struct CreateLeagueResponse {
    pub id: i32
}

#[derive(Debug, Clone, Deserialize)]
pub struct UpdateLeagueRequest {
    pub name: String,
    pub information: Option<String>,
    pub sport: String,
    pub matchup_locations: Vec<UpdateMatchupLocation>,
    pub administrators: Vec<UpdateLeagueAdministrator>,
    pub organization: String,
    pub contact_phone: String,
    pub games_per_matchup: u8
}

#[derive(Debug, Clone, Deserialize)]
pub struct UpdateMatchupLocation {
    pub id: Option<i32>,
    pub name: String
}

#[derive(Debug, Clone, Deserialize)]
pub struct UpdateLeagueAdministrator {
    pub id: Option<i32>,
    pub user_id: i32
}

#[derive(Debug, Clone, Serialize)]
pub struct GetLeagueResponse {
    pub id: i32,
    pub name: String,
    pub information: Option<String>,
    pub sport: String,
    pub matchup_locations: Vec<MatchupLocation>,
    pub organization: String,
    pub contact_phone: String,
    pub administrators: Vec<Administrator>,
    pub team_count: i32,
    pub games_per_matchup: i32
}

#[derive(Debug, Clone, Serialize)]
pub struct MatchupLocation {
    pub id: i32,
    pub name: String
}

#[derive(Debug, Clone, Serialize)]
pub struct Administrator {
    pub id: i32,
    pub user_id: i32,
    pub first_name: String,
    pub last_name: String
}

#[derive(Debug, Clone, Serialize)]
pub struct SearchLeaguesResponseItem {
    pub id: i32,
    pub name: String,
    pub information: Option<String>,
    pub sport: String,
    pub organization: String,
    pub team_count: i32
}
