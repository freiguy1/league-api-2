use rocket_contrib::Json;
use std::collections::HashSet;
use std::ops::DerefMut;

use error::Error;
use guards::{DbConn, Cache, LeagueAdmin};
use response::LeagueResponse;
use super::contract::UpdateLeagueRequest;
use league_db::repo::{
    TeamEventRepo, UserRepo, UpdateLeague, LeagueRepo, LeagueDao,
    MatchupLocation, Administrator, NewMatchupLocation, NewAdministrator
};

type Response = Result<LeagueResponse<()>, Error>;

#[put("/<league_id>", data = "<update_league_request>")]
pub fn handle(league_id: i32, mut update_league_request: Json<UpdateLeagueRequest>, conn: DbConn, cache: Cache, _user_id: LeagueAdmin) -> Response {
    let league_repo = LeagueRepo::new(&conn);
    let team_event_repo = TeamEventRepo::new(&conn);
    let user_repo = UserRepo::new(&conn);
    let current_league = match league_repo.get_single(league_id)? {
        Some(l) => l,
        None => return Ok(LeagueResponse::not_found())
    };
    let reference = ::util::Reference::new(cache.clone(), &conn);
    let sports = reference.sports();
    let request_user_ids: HashSet<i32> = update_league_request.administrators.iter().map(|admin| admin.user_id).collect();
    let request_users = user_repo.get_many(&request_user_ids.iter().map(|id| *id).collect())?;
    let team_events = team_event_repo.get_by_league_ids(&vec![league_id])?;
    let used_location_ids: HashSet<i32> = team_events.into_iter()
        .filter_map(|te| te.matchup())
        .map(|g| g.matchup_location_id)
        .collect();

    let validation_errors = super::validator::validate(update_league_request.deref_mut(), &sports, &used_location_ids, Some(&current_league), &request_users);
    if !validation_errors.is_empty() {
        return Ok(LeagueResponse::bad_request(validation_errors));
    }

    let dao = build_update_dao(league_id, update_league_request.into_inner(), sports, current_league);

    league_repo.update(&dao)?;

    Ok(LeagueResponse::success(()))
}

fn build_update_dao(league_id: i32,
                    req: UpdateLeagueRequest,
                    sports: Vec<::util::KeyValue>,
                    current_league: LeagueDao) -> UpdateLeague {
    let sport_id = sports
        .iter()
        .filter(|s| s.value.to_lowercase() == req.sport.to_lowercase())
        .next()
        .unwrap().key;

    // location shenanigans
    let request_location_ids: HashSet<i32> = req.matchup_locations.iter().filter_map(|loc| loc.id).collect();
    let current_location_ids: HashSet<i32> = current_league.matchup_locations.iter().map(|loc| loc.id).collect();
    let remove_matchup_locations: Vec<i32> = current_location_ids.difference(&request_location_ids).map(|&id| id).collect();
    let update_matchup_locations: Vec<MatchupLocation> = req.matchup_locations.iter()
        .filter(|loc| loc.id.is_some())
        .filter(|loc| {
            let current_loc = current_league.matchup_locations.iter().find(|gl| gl.id == loc.id.unwrap()).unwrap();
            loc.name != current_loc.name
        }).map(|loc| MatchupLocation {
            id: loc.id.unwrap(),
            name: loc.name.clone()
        }).collect();
    let new_matchup_locations: Vec<NewMatchupLocation> = req.matchup_locations.iter()
        .filter(|loc| loc.id.is_none())
        .map(|loc| NewMatchupLocation { name: loc.name.clone() }).collect();

    // admin shenanigans
    let request_admin_ids: HashSet<i32> = req.administrators.iter().filter_map(|ad| ad.id).collect();
    let current_admin_ids: HashSet<i32> = current_league.administrators.iter().map(|ad| ad.id).collect();
    let remove_administrators: Vec<i32> = current_admin_ids.difference(&request_admin_ids).map(|&id| id).collect();
    let update_administrators: Vec<Administrator> = req.administrators.iter()
        .filter(|ad| ad.id.is_some())
        .filter(|ad| {
            let current_ad = current_league.administrators.iter().find(|ad2| ad2.id == ad.id.unwrap()).unwrap();
            ad.user_id != current_ad.user_id
        }).map(|ad| Administrator {
            id: ad.id.unwrap(),
            user_id: ad.user_id
        }).collect();
    let new_administrators: Vec<NewAdministrator> = req.administrators.iter()
        .filter(|ad| ad.id.is_none())
        .map(|ad| NewAdministrator { user_id: ad.user_id }).collect();

    UpdateLeague {
        id: league_id,
        name: req.name,
        information: req.information,
        sport_id: sport_id,
        organization: req.organization.clone(),
        contact_phone: req.contact_phone.clone(),
        remove_matchup_locations: remove_matchup_locations,
        update_matchup_locations: update_matchup_locations,
        new_matchup_locations: new_matchup_locations,
        remove_administrators: remove_administrators,
        update_administrators: update_administrators,
        new_administrators: new_administrators,
        games_per_matchup: req.games_per_matchup as i32
    }
}
