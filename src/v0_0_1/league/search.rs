use rocket_contrib::Json;
use std::collections::HashMap;

use error::Error;
use guards::{DbConn, Cache};
use league_db::repo::{LeagueRepo, LeagueDao, TeamRepo};
use super::contract::SearchLeaguesResponseItem;
use response::LeagueResponse;

type Response = Result<LeagueResponse<Json<Vec<SearchLeaguesResponseItem>>>, Error>;

#[get("/search?<query>")]
pub fn handle(query: Query, conn: DbConn, cache: Cache) -> Response {
    // If query contains nothing, improper search
    if !query.is_valid() {
        let errors = vec!["query must be longer than 2 characters"];
        return Ok(LeagueResponse::bad_request(errors));
    }

    let repo = LeagueRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);
    let reference = ::util::Reference::new(cache.clone(), &conn);
    let sports = reference.sports();

    let offset = (query.page_number() - 1) * query.page_size();
    let daos = repo.search(&query.q, query.page_size(), offset)?;
    let league_ids = daos.iter().map(|d| d.id).collect();
    let team_counts = team_repo.get_count_by_league_ids(&league_ids)?;
    let leagues = daos.into_iter().map(|dao|build_response(dao, &sports, &team_counts)).collect::<Vec<_>>();
    Ok(LeagueResponse::success(Json(leagues)))
}

#[derive(FromForm)]
pub struct Query {
    q: String,
    page_number: Option<usize>,
    page_size: Option<usize>
}

impl Query {
    fn is_valid(&self) -> bool {
        self.q_is_valid()
    }

    fn q_is_valid(&self) -> bool {
        self.q.len() > 2
    }

    fn page_number(&self) -> usize {
        self.page_number.unwrap_or(1)
    }

    fn page_size(&self) -> usize {
        self.page_size.unwrap_or(10)
    }
}

fn build_response(dao: LeagueDao,
                  sports: &Vec<::util::KeyValue>,
                  team_counts: &HashMap<i32, i32>) -> SearchLeaguesResponseItem {
    let sport = sports.iter().filter(|s| s.key == dao.sport_id).next().unwrap().value.clone();

    SearchLeaguesResponseItem {
        id: dao.id,
        name: dao.name,
        information: dao.information,
        sport: sport,
        organization: dao.organization,
        team_count: team_counts[&dao.id]
    }
}
