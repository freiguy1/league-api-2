use rocket_contrib::Json;
use std::collections::HashSet;
use std::ops::DerefMut;

use error::Error;
use guards::{DbConn, Cache, UserRequired};
use response::LeagueResponse;
use super::contract::{CreateLeagueRequest, CreateLeagueResponse};
use league_db::repo::{LeagueRepo, NewLeague, NewMatchupLocation, NewAdministrator};

type Response = Result<LeagueResponse<Json<CreateLeagueResponse>>, Error>;

#[post("/", data = "<create_league_request>")]
pub fn handle(mut create_league_request: Json<CreateLeagueRequest>, conn: DbConn, cache: Cache, user_id: UserRequired) -> Response {
    let league_repo = LeagueRepo::new(&conn);
    let reference = ::util::Reference::new(cache.clone(), &conn);
    let sports = reference.sports();

    let validation_errors = super::validator::validate(create_league_request.deref_mut(), &sports, &HashSet::new(), None, &vec![]);
    if !validation_errors.is_empty() {
        return Ok(LeagueResponse::bad_request(validation_errors));
    }

    let league_dao = build_league_dao(&create_league_request, &sports, user_id.0);

    let created_league_id = league_repo.create(&league_dao)?;

    let response = CreateLeagueResponse {
        id: created_league_id
    };

    // match context.emailer() {
    //     Some(e) => {
    //         e.send_async(
    //             "ethan.frei@gmail.com",
    //             "Master Mind",
    //             "A new league was created",
    //             &format!("League '{}' created with id: {:?}", league_dao.name, created_league_id));
    //     },
    //     None => {}
    // }

    // Return response
    Ok(LeagueResponse::success(Json(response)))
}

fn build_league_dao(req: &CreateLeagueRequest, sports: &Vec<::util::KeyValue>, admin_user_id: i32) -> NewLeague {
    let sport_id = sports
        .iter()
        .filter(|s| s.value.to_lowercase() == req.sport.to_lowercase())
        .next()
        .unwrap().key;
    NewLeague {
        name: req.name.clone(),
        information: req.information.clone(),
        sport_id: sport_id,
        organization: req.organization.clone(),
        contact_phone: req.contact_phone.clone(),
        matchup_locations: req.matchup_locations.iter().map(|loc| NewMatchupLocation {
            name: loc.clone()
        }).collect(),
        administrators: vec![NewAdministrator {
            user_id: admin_user_id
        }],
        games_per_matchup: req.games_per_matchup as i32
    }
}
