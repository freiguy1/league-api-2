use rocket_contrib::Json;

use error::Error;
use guards::{DbConn, Cache, UserOptional};
use league_db::repo::{TeamRepo, LeagueRepo, UserRepo};
use super::contract::{MatchupLocation, GetLeagueResponse, Administrator};
use response::LeagueResponse;

type Response = Result<LeagueResponse<Json<GetLeagueResponse>>, Error>;

#[get("/<league_id>")]
pub fn handle(league_id: i32, conn: DbConn, cache: Cache, user_id: UserOptional) -> Response {

    let league_repo = LeagueRepo::new(&conn);
    let dao = match league_repo.get_single(league_id)? {
        Some(league_dao) => league_dao,
        None => return Ok(LeagueResponse::not_found())
    };
    let reference = ::util::Reference::new(cache.clone(), &conn);
    let sports = reference.sports();
    let sport = sports.iter().filter(|s| s.key == dao.sport_id).next().unwrap().value.clone();

    let team_repo = TeamRepo::new(&conn);
    let team_count = team_repo.get_count_by_league_ids(
        &vec![league_id].into_iter().collect())?[&league_id];

    let user_ids : Vec<i32> = dao.administrators.iter().map(|ad| ad.user_id).collect();
    let is_admin = user_id.map(|uid1| user_ids.iter().any(|&uid2| uid1 == uid2)).unwrap_or(false);
    let administrators = if is_admin {
        let user_repo = UserRepo::new(&conn);
        let users = user_repo.get_many(&user_ids)?;
        dao.administrators.into_iter().map(|ad|{
            let user = users.iter().find(|u| u.id == ad.user_id).unwrap();
            Administrator {
                id: ad.id,
                user_id: ad.user_id,
                first_name: user.first_name.clone(),
                last_name: user.last_name.clone(),
            }
        }).collect()
    } else {
        Vec::new()
    };

    let response = GetLeagueResponse {
        id: dao.id,
        name: dao.name,
        information: dao.information,
        organization: dao.organization,
        contact_phone: dao.contact_phone,
        games_per_matchup: dao.games_per_matchup,
        sport: sport,
        matchup_locations: dao.matchup_locations.into_iter().map(|loc| MatchupLocation {
            id: loc.id,
            name: loc.name
        }).collect(),
        administrators: administrators,
        team_count: team_count,
    };

    Ok(LeagueResponse::success(Json(response)))
}
