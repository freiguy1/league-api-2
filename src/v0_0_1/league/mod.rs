use rocket::Rocket;

mod contract;
mod search;
mod get_by_id;
mod create;
mod update;

mod validator;

pub fn init_routes(rocket: Rocket) -> Rocket {
    let routes = routes![
        search::handle,
        get_by_id::handle,
        create::handle,
        update::handle
    ];
    rocket.mount(&format!("/{}/leagues/", super::VER), routes)
}