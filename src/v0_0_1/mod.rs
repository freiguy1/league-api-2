use rocket::Rocket;

mod league;
mod team;
mod my;
mod team_event;

static VER: &'static str = "v0.0.1";

pub fn init_routes(rocket: Rocket) -> Rocket {
    let mut rocket = league::init_routes(rocket);
    rocket = team::init_routes(rocket);
    rocket = team_event::init_routes(rocket);
    my::init_routes(rocket)
}