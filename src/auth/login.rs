use super::contract::{LoginRequest, LoginResponse};
use guards::{Config, DbConn};
use rocket_contrib::Json;
use league_db::repo::{PasswordRepo, UserRepo};
use error::Error;
use pwhash::bcrypt;
use response::LeagueResponse;


#[post("/login", data = "<login_request>")]
pub fn handle(login_request: Json<LoginRequest>, conn: DbConn, config: Config) -> Result<LeagueResponse<Json<LoginResponse>>, Error> {
    let user_repo = UserRepo::new(&conn);
    let password_repo = PasswordRepo::new(&conn);

    let user_from_db = match user_repo.get_by_email(&login_request.email.to_lowercase())? {
        Some(user) => user,
        None => return Ok(LeagueResponse::bad_request(vec!["invalid email password combo"]))
    };

    let password_id = match user_from_db.user_password_id {
        Some(id) => id,
        None => return Ok(LeagueResponse::bad_request(vec!["invalid email password combo"]))
    };

    let password_from_db = password_repo.get_single(password_id)?.unwrap();

    let pw_attempt_and_salt = format!("{}{}", login_request.password, password_from_db.salt);

    if bcrypt::verify(&pw_attempt_and_salt, &password_from_db.password) {
        let token = super::create_token(
            user_from_db.id,
            &config);

        let login_response = LoginResponse {
            token: token.clone()
        };
        Ok(LeagueResponse::add_token(token, Json(login_response)))
    } else {
        Ok(LeagueResponse::bad_request(vec!["invalid email password combo"]))
    }

}
