use super::contract::{CreateUserRequest, CreateUserResponse};
use guards::{Config, DbConn};
use rocket_contrib::Json;
use league_db::repo::{PasswordRepo, UserRepo, OAuth2InfoRepo,
                      NewPassword, NewUser, UserDao, OAuth2InfoDao};
use error::Error;
use pwhash::bcrypt;
use rand::Rng;
use response::LeagueResponse;

type Response = Result<LeagueResponse<Json<CreateUserResponse>>, Error>;

#[post("/", data = "<create_user_request>")]
pub fn handle(mut create_user_request: Json<CreateUserRequest>,
              conn: DbConn,
              config: Config) -> Response {
    let validation_errors = create_user_request.validate();
    if !validation_errors.is_empty() {
        return Ok(LeagueResponse::bad_request(validation_errors))
    }

    let user_repo = UserRepo::new(&conn);
    let password_repo = PasswordRepo::new(&conn);
    let oauth2_info_repo = OAuth2InfoRepo::new(&conn);
    // TODO: All the logging things.
    let logger = ::util::Logger::new(&conn);

    if user_repo.get_by_email(&create_user_request.email)?.is_some() {
        return Ok(LeagueResponse::bad_request(vec!["User already exists with email"]))
    }

    // Validate that oauth2_info_id, if exists, maps to valid oauth2_info row
    let oauth2_info = if let Some(oauth2_info_id) = create_user_request.oauth2_info_id {
        if let Some(oauth2_info) = oauth2_info_repo.get_by_id(oauth2_info_id)? {
            if oauth2_info.user_id.is_some() {
                logger.log_simple(
                    &format!("{:?}", create_user_request),
                    "attempted to create user where oauth2_info already has user_id",
                    ::util::Severity::Warning);
                return Ok(LeagueResponse::bad_request(vec!["Invalid oauth2_info_id"]))
            } else {
                Some(oauth2_info)
            }
        } else {
            logger.log_simple(
                &format!("{:?}", create_user_request),
                "attempted to create user where oauth2_info doesn't exist by oauth2_info_id",
                ::util::Severity::Warning);
            return Ok(LeagueResponse::bad_request(vec!["Invalid oauth2_info_id"]))
        }
    } else { None };

    // Create password row if needed
    let password_id = if let Some(ref password) = create_user_request.password {
        let password_info_dao = create_password_dao(&password);
        let password_id = password_repo.create(&password_info_dao)?.id;
        Some(password_id)
    } else { None };

    // User is only confirmed if the email matches their oauth2_info email
    let is_confirmed = oauth2_info.as_ref().map(|oi| oi.email == create_user_request.email).unwrap_or(false);
    let user_dao = create_user_dao(&create_user_request, password_id, is_confirmed);
    let user_from_db = user_repo.create(&user_dao)?;

    // Fill in user_id in oauth2_info
    if let Some(ref oauth2_info) = oauth2_info {
        oauth2_info_repo.update_user_id(oauth2_info.id, Some(user_from_db.id))?;
    }

    log_results(&logger, &user_from_db, &oauth2_info);

    // if let Some(ref emailer) = context.emailer() {
    //     send_user_email(emailer, &user_from_db, is_confirmed);
    // }

    let token = super::create_token(user_from_db.id, &config);

    let create_user_response = CreateUserResponse {
        token: token.clone()
    };
    Ok(LeagueResponse::add_token(token, Json(create_user_response)))
}

fn log_results(logger: &::util::Logger, user_dao: &UserDao, oauth2_info: &Option<OAuth2InfoDao>) {
    let description = if let &Some(ref oauth2_info) = oauth2_info {
        format!("New user created with oauth2 provider {}", oauth2_info.provider)
    } else {
        "New user created with username password method".to_string()
    };
    logger.log_simple(
        &format!("user_id: {:?}", user_dao.id),
        &description,
        ::util::Severity::Info);
}

// fn send_user_email(emailer: &::util::Emailer, user_dao: &UserDao, is_confirmed: bool) {
//     if is_confirmed {
//         emailer.send_async(
//             &user_dao.email,
//             &format!("{} {}", user_dao.first_name, user_dao.last_name),
//             // "Please confirm email",
//             &format!("Welcome {}!", user_dao.first_name),
//             "Welcome to JustPlay. You successfully signed up through a federated authentication method");
//             // &user_dao.confirmation_key);
//     } else {
//         emailer.send_async(
//             &user_dao.email,
//             &format!("{} {}", user_dao.first_name, user_dao.last_name),
//             // "Please confirm email",
//             &format!("Welcome {}!", user_dao.first_name),
//             "One day you'll get an email with a link to confirm account creation. That day is not this day.");
//             // &user_dao.confirmation_key);
//     }
// }

fn create_user_dao(request: &CreateUserRequest, password_id: Option<i32>, _is_confirmed: bool) -> NewUser {
    NewUser {
        first_name: request.first_name.clone(),
        last_name: request.last_name.clone(),
        email: request.email.clone(),
        phone: request.phone.clone(),
        user_password_id: password_id,
        confirmation_key: ::uuid::Uuid::new_v4().hyphenated().to_string(),
        created_datetime: ::chrono::Utc::now().naive_utc(),
        // is_confirmed: is_confirmed
        is_confirmed: true // todo: implement user confirmation when website comes around
    }
}

fn create_password_dao(plain_password: &String) -> NewPassword {
    let salt = create_salt(30);
    let hashed_password = hash_password(plain_password, &salt);
    NewPassword {
        salt: salt,
        password: hashed_password
    }
}

fn hash_password(plain_password: &String, salt: &String) -> String {
    let password_and_salt = format!("{}{}", plain_password, salt);
    bcrypt::hash(&password_and_salt).unwrap()
}

fn create_salt(len: usize) -> String {
    ::rand::thread_rng()
        .gen_ascii_chars()
        .take(len)
        .collect()
}

