use hyper::Client;
use hyper::header::{Authorization, Bearer};
use hyper::net::HttpsConnector;
use hyper_native_tls::NativeTlsClient;
use oauth2::Config;
use url::Url;
use std::io::Read;
use super::{OAuth2Strategy, OAuth2ProfileInfo};
use error::Error;

pub const CALLBACK_PATH: &'static str = "/auth/google/callback";
pub const NAME: &'static str = "google";
const AUTH_URL: &'static str = "https://accounts.google.com/o/oauth2/v2/auth";
const TOKEN_URL: &'static str = "https://www.googleapis.com/oauth2/v3/token";
const PROFILE_SCOPE: &'static str = "https://www.googleapis.com/auth/plus.me";
const EMAIL_SCOPE: &'static str = "https://www.googleapis.com/auth/userinfo.email";
const PROFILE_REQUEST_URL: &'static str = "https://content.googleapis.com/plus/v1/people/me";

pub struct GoogleOAuth2Strategy {
    oauth2_config: Config
}

impl GoogleOAuth2Strategy {
    pub fn new(client_id: &str,
           client_secret: &str,
           base_url: &Url) -> GoogleOAuth2Strategy {
        let mut oauth2_config = Config::new(client_id, client_secret, AUTH_URL, TOKEN_URL);
        oauth2_config = oauth2_config.add_scope(PROFILE_SCOPE);
        oauth2_config = oauth2_config.add_scope(EMAIL_SCOPE);
        let redirect_url = base_url.join(CALLBACK_PATH).expect("unable to create google redirect_url");
        oauth2_config = oauth2_config.set_redirect_url(redirect_url.to_string());
        GoogleOAuth2Strategy {
            oauth2_config: oauth2_config
        }
    }
}

impl OAuth2Strategy for GoogleOAuth2Strategy {
    fn get_token(&self, code: String, postmessage: bool) -> Result<String, Error>  {
        let token_result = if postmessage {
            self.oauth2_config.clone().set_redirect_url("postmessage").exchange_code(code.clone())
        } else {
            self.oauth2_config.exchange_code(code.clone())
        };
        let token = token_result.map_err(
            |e| Error::only_desc(
                &e.error_description .unwrap_or("Google oauth2 error when trying to get token".to_string())
            )
        )?;
        Ok(token.access_token)
    }

    fn authorize_url(&self) -> String {
        self.oauth2_config.authorize_url().to_string()
    }

    fn get_profile_info(&self, token: String) -> Result<OAuth2ProfileInfo, Error> {
        let authorization_header = Authorization(
            Bearer {
                token: token.clone()
            }
        );

        let ssl = NativeTlsClient::new().unwrap();
        let connector = HttpsConnector::new(ssl);
        let client = Client::with_connector(connector);
        let request = client.get(PROFILE_REQUEST_URL).header(authorization_header);

        let mut response = request.send()?;
        let mut body = String::new();
        response.read_to_string(&mut body)?;

        let google_profile_info: GoogleProfileInfo = ::serde_json::from_str(&body)?;
        let email = match google_profile_info.emails.iter().find(|e| e._type == "account".to_string()) {
            Some(e) => e.value.clone(),
            None => return Err(Error::only_desc("Unable get email from google profile info"))

        };
        Ok(OAuth2ProfileInfo {
            id: google_profile_info.id,
            first_name: google_profile_info.name.given_name,
            last_name: google_profile_info.name.family_name,
            email: email,
            phone: None
        })
    }
}

#[derive(Deserialize, Debug)]
struct GoogleProfileInfo {
    id: String,
    emails: Vec<GoogleEmail>,
    name: GoogleName
}

#[derive(Deserialize, Debug)]
struct GoogleEmail {
    value: String,
    #[serde(rename = "type")]
    _type: String
}

#[derive(Deserialize, Debug)]
struct GoogleName {
    #[serde(rename = "familyName")]
    family_name: String,
    #[serde(rename = "givenName")]
    given_name: String
}
