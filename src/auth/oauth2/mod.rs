use rocket::Rocket;
use rocket_contrib::Json;

use error::Error;
use guards::{DbConn, Config};
use league_db::repo::{OAuth2InfoRepo, OAuth2InfoDao, UserRepo, UserDao};
use response::LeagueResponse;
use super::contract::{OAuth2Response, NewUserFromOAuth2};

mod google;

struct OAuth2ProfileInfo {
    id: String,
    email: String,
    first_name: String,
    last_name: String,
    phone: Option<String>
}

trait OAuth2Strategy: Send + Sync + ::std::any::Any + 'static{
    fn get_token(&self, code: String, postmessage: bool) -> Result<String, Error>;
    fn get_profile_info(&self, token: String) -> Result<OAuth2ProfileInfo, Error>;
    fn authorize_url(&self) -> String;
}

pub fn init_routes(rocket: Rocket) -> Rocket {
    let routes = routes![
        google_handle,
        list_oauth2_handler
    ];
    rocket.mount("/", routes)
}


type Response = Result<LeagueResponse<Json<OAuth2Response>>, Error>;

struct OAuth2Callback<OS: OAuth2Strategy> {
    strategy: OS,
    name: &'static str
}

impl<OS: OAuth2Strategy> OAuth2Callback<OS> {
    fn get_oauth2_info(&self, conn: &DbConn, profile_info: &OAuth2ProfileInfo) -> Result<Option<OAuth2InfoDao>, Error> {
        let oauth2_info_repo = OAuth2InfoRepo::new(&conn);
        let row = oauth2_info_repo.get_by_provider_profile_id(self.name, &profile_info.id)?;
        Ok(row)
    }

    fn get_user(&self, conn: &DbConn, profile_info: &OAuth2ProfileInfo) -> Result<Option<UserDao>, Error> {
        let user_repo = UserRepo::new(&conn);
        let row = user_repo.get_by_email(&profile_info.email)?;
        Ok(row)
    }

    fn create_oauth2_info(&self, conn: &DbConn, profile_info: &OAuth2ProfileInfo, user_id: Option<i32>) -> Result<i32, Error> {
        let oauth2_info_repo = OAuth2InfoRepo::new(&conn);
        let result = oauth2_info_repo.create(self.name, &profile_info.id, &profile_info.email, user_id)?;
        Ok(result.id)
    }

    fn create_user_response(profile_info: OAuth2ProfileInfo, oauth2_info_id: i32) -> Response {
        let new_user = NewUserFromOAuth2 {
            email: profile_info.email,
            first_name: profile_info.first_name,
            last_name: profile_info.last_name,
            phone: profile_info.phone,
            oauth2_info_id: oauth2_info_id
        };
        let response_obj = OAuth2Response { token: None, new_user: Some(new_user) };
        Ok(LeagueResponse::success(Json(response_obj)))
    }

    fn create_successful_response(token: String) -> Response {
        let response_obj = OAuth2Response { token: Some(token.clone()), new_user: None };
        Ok(LeagueResponse::add_token(token, Json(response_obj)))
    }

    fn handle(&self, code: String, postmessage: bool, conn: DbConn, config: &Config) -> Response {
        let token = self.strategy.get_token(code, postmessage)?;
        let profile_info = self.strategy.get_profile_info(token)?;

        if let Some(oauth2_info) = self.get_oauth2_info(&conn, &profile_info)? {
            if let Some(user_id) = oauth2_info.user_id {
                // OAuth2Info exists with user id
                let token = super::create_token(user_id, config);
                Self::create_successful_response(token)
            } else {
                // OAuth2Info exists with no user id (not that common)
                Self::create_user_response(profile_info, oauth2_info.id)
            }
        } else if let Some(user) = self.get_user(&conn, &profile_info)? {
            // User exists by email but not oauth2info
            self.create_oauth2_info(&conn, &profile_info, Some(user.id))?;
            let token = super::create_token(user.id, config);
            Self::create_successful_response(token)
        } else {
            // User doesn't exist at all
            let oauth2_info_id = self.create_oauth2_info(&conn, &profile_info, None)?;
            Self::create_user_response(profile_info, oauth2_info_id)
        }
    }

}

// This might just be temporary. If now, it needs to be better.
#[get("/auth/oauth2")]
pub fn list_oauth2_handler(config:Config) -> Json<Vec<String>> {
    let google_auth = config.google_auth.as_ref().unwrap();
    let google_strategy = google::GoogleOAuth2Strategy::new(
        &google_auth.client_id,
        &google_auth.client_secret,
        &config.base_url);

    Json(vec![google_strategy.authorize_url()])
}

// ------------------------
// GOOGLE CALLBACK HANDLER
// ------------------------

#[derive(FromForm)]
pub struct CallbackQueryString {
    code: String,
    postmessage: Option<bool>
}

#[get("/auth/google/callback?<query>")]
pub fn google_handle(query: CallbackQueryString, conn: DbConn, config: Config) -> Response {
    // Could maybe throw this func into a struct which holds a premade handler
    // instead of making one every time, even though I don't think it's too
    // expensive
    if let Some(ref google_auth) = config.google_auth {
        let google_strategy = google::GoogleOAuth2Strategy::new(
            &google_auth.client_id,
            &google_auth.client_secret,
            &config.base_url);

        // authorize_urls.push(AuthorizeUrlResponseItem {
        //     provider: google::NAME,
        //     url: google_strategy.authorize_url()
        // });

        let google_handler = OAuth2Callback {
            strategy: google_strategy,
            name: google::NAME
        };

        google_handler.handle(query.code, query.postmessage.unwrap_or(false), conn, &config)
    } else {
        Ok(LeagueResponse::not_found())
    }
}