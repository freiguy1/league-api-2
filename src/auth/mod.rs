use rocket::Rocket;
use chrono::*;
use jwt::{Header, Token, Registered};
use crypto::sha2::Sha256;

mod login;
mod contract;
mod create_user;
mod oauth2;

pub fn init_routes(rocket: Rocket) -> Rocket {
    let rocket = oauth2::init_routes(rocket);

    let routes = routes![
        login::handle,
        create_user::handle
    ];
    rocket.mount("user/", routes)
}

fn create_token(user_id: i32, config: &::config::Config) -> String {
    let expiration_time = Utc::now() + Duration::minutes(config.token_expiration_minutes as i64);
    let exp_micros = expiration_time.timestamp() as u64;
    let header: Header = Default::default();
    let claims = Registered {
        iss: Some(config.app_name.clone()),
        sub: Some(user_id.to_string()),
        exp: Some(exp_micros),
        ..Default::default()
    };
    let token = Token::new(header, claims);

    token.signed(config.secret.as_ref(), Sha256::new()).unwrap()
}

pub fn user_id_from_token(token: &str, secret: &String) -> Option<i32> {
    let token = match Token::<Header, Registered>::parse(&token) {
        Ok(token) => token,
        Err(_) => return None
    };

    if !token.verify(secret.as_ref(), Sha256::new()) {
        return None;
    }

    if token.claims.exp
        .map(|exp| NaiveDateTime::from_timestamp(exp as i64, 0))
        .map(|exp| DateTime::from_utc(exp, Utc))
        .map(|exp| Utc::now() > exp)
        .unwrap_or(true) {
        return None;
    }

    token.claims.sub.and_then(|sub| sub.parse().ok())
}
