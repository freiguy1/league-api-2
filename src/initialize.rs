use diesel::pg::PgConnection;
use r2d2_diesel::ConnectionManager;
use config::Config;
use rocket::http::Method;
use r2d2::Pool;
use rocket_cors::{AllowedOrigins, Cors};
use util::Cache;
use std::sync::{Arc, Mutex};

type Conn = ::r2d2::Pool<ConnectionManager<PgConnection>>;

pub fn init_pool(config: &Config) -> Conn {
    let manager = ConnectionManager::<PgConnection>::new(config.database_url.clone());
    Pool::builder().build(manager).expect("Could not initialize database pool")
    // Pool::new(r2d2_config, manager).expect("Could not initialize database pool")
}

pub fn init_cors(config: &Config) -> Cors {
    let cors_domains: Vec<&str> = config.cors_domains
        .iter()
        .map(|cd| cd.as_ref())
        .collect();
    let (allowed_origins, _) = AllowedOrigins::some(&cors_domains.as_slice());

    Cors {
        allowed_origins: allowed_origins,
        allowed_methods: vec![Method::Get, Method::Post, Method::Delete, Method::Put].into_iter().map(From::from).collect(),
        // allowed_headers: AllowedHeaders::some(&["Authorization", "Accept", "Accept-Encoding", "Accept-Language", "Connection", "Host", "Origin", "User-Agent"]),
        allow_credentials: true,
        ..Default::default()
    }
}

pub fn init_cache(config: &Config) -> Arc<Mutex<Cache>> {
    let cache = Cache::new(::chrono::Duration::minutes(config.cache_expiration_minutes));
    Arc::new(Mutex::new(cache))
}
