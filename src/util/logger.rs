use chrono::Utc;
use guards::DbConn;
use league_db::repo::{NewLog, LogRepo};

#[allow(dead_code)]
pub enum Severity { Info, Warning, Error }

impl Severity {
    fn value(&self) -> i32 {
        match *self {
            Severity::Info => 0,
            Severity::Warning => 1,
            Severity::Error => 2
        }
    }
}

pub struct Logger<'a> {
    conn: &'a DbConn
}

impl <'a> Logger<'a> {

    pub fn new(conn: &'a DbConn) -> Logger<'a> {
        Logger { conn: conn }
    }

    // pub fn log_with_request_error<E: StdError>(&self, req: &mut Request, err: &E) {
    //     let context = req.extensions.get::<::context::ApiContext>().unwrap();

    //     let mut body = String::new();
    //     // Swallow this error because the show must go on.
    //     let _ = req.body.read_to_string(&mut body);

    //     let description = match err.cause() {
    //         Some(ref c) => format!("Description: {}\r\nCause Description: {}",
    //                                err.description(),
    //                                c.description()),
    //         None => format!("Description: {}", err.description())
    //     };

    //     let new_log = NewLog {
    //         context: if body.len() > 500 { body[..500].to_string() } else { body },
    //         description: description,
    //         datetime: Utc::now().naive_utc(),
    //         severity: Severity::Error.value(),
    //         http_verb: Some(format!("{:?}", req.method)),
    //         request_url: Some(req.url.to_string()),
    //         authenticated_user_id: context.user_id
    //     };

    //     let repo = LogRepo::new(self.conn);
    //     let _ = repo.create(&new_log);
    // }

    pub fn log_simple(&self,
        context: &str,
        description: &str,
        severity: Severity) {
        self.log_custom(context, description, severity, None, None, None);
    }

    pub fn log_custom(&self,
        context: &str,
        description: &str,
        severity: Severity,
        http_verb: Option<String>,
        request_url: Option<String>,
        authenticated_user_id: Option<i32>) {
        let new_log = NewLog {
            context: context.to_string(),
            description: description.to_string(),
            datetime: Utc::now().naive_utc(),
            severity: severity.value(),
            http_verb: http_verb,
            request_url: request_url,
            authenticated_user_id: authenticated_user_id
        };

        let repo = LogRepo::new(&self.conn);
        let _ = repo.create(&new_log);
    }

}
