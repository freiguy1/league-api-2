use std::sync::{Arc, Mutex};
use league_db::repo::ReferenceRepo;
use diesel::pg::PgConnection;
use ::util::Cache;

#[derive(Serialize, Deserialize, Clone)]
pub struct KeyValue {
    pub key: i32,
    pub value: String
}

impl KeyValue {
    fn new(key: i32, value: String) -> KeyValue {
        KeyValue { key: key, value: value }
    }
}

pub struct Reference<'a> {
    cache: Arc<Mutex<Cache>>,
    repo: ReferenceRepo<'a>
}

impl <'a> Reference<'a> {
    pub fn new(cache: Arc<Mutex<Cache>>, conn: &'a PgConnection) -> Reference {
        Reference{ cache: cache, repo: ReferenceRepo::new(conn) }
    }

    // TODO: Add better errors
    pub fn sports(&self) -> Vec<KeyValue> {
        let mut cache = self.cache.lock().unwrap();
        if let Some(sports) = cache.get::<Vec<KeyValue>>("sports".to_string()) {
            sports
        } else {
            let sports: Vec<KeyValue> = self.repo.get_sports()
                .expect("could not retrieve sports")
                .into_iter()
                .map(|sport| KeyValue::new(sport.id, sport.name))
                .collect();
            cache.insert("sports".to_string(), &sports);
            sports
        }

    }

    // TODO: Add better errors
    pub fn matchup_statuses(&self) -> Vec<KeyValue> {
        let mut cache = self.cache.lock().unwrap();
        if let Some(matchup_statuses) = cache.get::<Vec<KeyValue>>("matchup_statuses".to_string()) {
            matchup_statuses
        } else {
            let matchup_statuses: Vec<KeyValue> = self.repo.get_matchup_statuses()
                .expect("could not retrieve matchup_statuses")
                .into_iter()
                .map(|matchup_statuses| KeyValue::new(matchup_statuses.id, matchup_statuses.name))
                .collect();
            cache.insert("matchup_statuses".to_string(), &matchup_statuses);
            matchup_statuses
        }
    }
}

