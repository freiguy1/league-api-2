use std::collections::{HashMap, HashSet};

#[derive(Deserialize, Serialize, Clone)]
pub struct Record {
    pub win: i32,
    pub loss: i32,
    pub tie: i32
}

impl Record {
    pub fn empty() -> Record {
        Record { 
            win: 0,
            loss: 0,
            tie: 0
        }
    }
}

enum MatchupResult {
    Win,
    Loss,
    Tie
}

pub fn calculate_records(matchups: &Vec<super::Matchup>, team_ids: &HashSet<i32>) -> HashMap<i32, Record> {
    let mut result = HashMap::new();
    for team_id in team_ids.iter() {
        let mut record = result.entry(*team_id).or_insert(Record { win: 0, loss: 0, tie: 0});
        for matchup in matchups.iter().filter(|g| (g.home_team_id == *team_id || g.away_team_id == *team_id) && g.is_complete()) {
            match get_result(*team_id, matchup) {
                MatchupResult::Win => record.win += 1,
                MatchupResult::Loss => record.loss += 1,
                MatchupResult::Tie => record.tie += 1
            }
        }
    }
    result
}

fn get_result(team_id: i32, matchup: &super::Matchup) -> MatchupResult {
    if matchup.is_tie() {
        MatchupResult::Tie
    } else if team_id == matchup.home_team_id && matchup.is_home_team_win() {
        MatchupResult::Win
    } else if team_id == matchup.away_team_id && matchup.is_away_team_win() {
        MatchupResult::Win
    } else {
        MatchupResult::Loss
    }
}

#[test]
fn test_calculate_records() {
    use super::Matchup;
    let mut matchups = Vec::new();
    matchups.push(Matchup { home_team_id: 1, away_team_id: 2, matchup_status_id: super::HOME_TEAM_WIN_ID });
    matchups.push(Matchup { home_team_id: 2, away_team_id: 1, matchup_status_id: super::HOME_TEAM_WIN_ID });
    matchups.push(Matchup { home_team_id: 1, away_team_id: 2, matchup_status_id: super::AWAY_TEAM_WIN_ID });
    matchups.push(Matchup { home_team_id: 3, away_team_id: 2, matchup_status_id: super::TIE_ID });
    matchups.push(Matchup { home_team_id: 1, away_team_id: 3, matchup_status_id: super::TIE_ID });
    matchups.push(Matchup { home_team_id: 1, away_team_id: 3, matchup_status_id: super::HOME_TEAM_FORFEIT_ID });
    matchups.push(Matchup { home_team_id: 2, away_team_id: 1, matchup_status_id: super::AWAY_TEAM_FORFEIT_ID });
    matchups.push(Matchup { home_team_id: 2, away_team_id: 1, matchup_status_id: 5 });
    matchups.push(Matchup { home_team_id: 2, away_team_id: 1, matchup_status_id: 15 });
    let result = calculate_records(&matchups, &vec![1, 2, 3].into_iter().collect());
    assert_eq!(result.len(), 3);
    assert!(result.contains_key(&1));
    assert!(result.contains_key(&2));
    assert!(result.contains_key(&3));
    assert_eq!(result[&1].win, 1);
    assert_eq!(result[&1].loss, 4);
    assert_eq!(result[&1].tie, 1);
    assert_eq!(result[&2].win, 3);
    assert_eq!(result[&2].loss, 1);
    assert_eq!(result[&2].tie, 1);
    assert_eq!(result[&3].win, 1);
    assert_eq!(result[&3].loss, 0);
    assert_eq!(result[&3].tie, 2);
}

