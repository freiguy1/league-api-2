
use std::error::Error as StdError;
use league_db::error::Error as DbError;
use serde_json::Error as SerdeJsonError;
use std::io::Error as IoError;
use hyper::Error as HyperError;

#[derive(Debug)]
pub struct Error {
    pub cause: Option<Cause>,
    pub description: String
}

impl Error {
    #[allow(dead_code)]
    pub fn new<C: Into<Error>>(cause: C, description: &str) -> Error {
        Error {
            cause: cause.into().cause,
            description: description.to_string()
        }
    }

    #[allow(dead_code)]
    pub fn only_desc(description: &str) -> Error {
        Error {
            cause: None,
            description: description.to_string()
        }
    }

    fn only_cause(cause: Cause) -> Error {
        let cause_description = format!("{} error occurred. Refer to cause.", cause.error_type_name());
        Error {
            cause: Some(cause),
            description: cause_description
        }
    }
}

#[derive(Debug)]
pub enum Cause {
    Db(DbError),
    SerdeJson(SerdeJsonError),
    Io(IoError),
    Hyper(HyperError)
}

impl Cause {
    /*
    fn description(&self) -> &str {
        match *self {
            Cause::Db(ref e) => e.description(),
            Cause::Email(ref e) => e.description(),
            Cause::Smtp(ref e) => e.description(),
            Cause::Hyper(ref e) => e.description(),
            Cause::SerdeJson(ref e) => e.description(),
            Cause::Io(ref e) => e.description()
        }
    }
    */

    fn error_type_name(&self) -> &'static str {
        match *self {
            Cause::Db(_) => "Database",
            Cause::SerdeJson(_) => "Serde",
            Cause::Hyper(_) => "Hyper",
            Cause::Io(_) => "IO"
        }
    }
}

impl From<DbError> for Error {
    fn from(e: DbError) -> Error {
        Error::only_cause(Cause::Db(e))
    }
}

impl From<SerdeJsonError> for Error {
    fn from(e: SerdeJsonError) -> Error {
        Error::only_cause(Cause::SerdeJson(e))
    }
}

impl From<IoError> for Error {
    fn from(e: IoError) -> Error {
        Error::only_cause(Cause::Io(e))
    }
}

impl From<HyperError> for Error {
    fn from(e: HyperError) -> Error {
        Error::only_cause(Cause::Hyper(e))
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        &self.description
    }

    fn cause(&self) -> Option<&StdError> {
        match self.cause {
            Some(Cause::Db(ref e)) => Some(e),
            Some(Cause::SerdeJson(ref e)) => Some(e),
            Some(Cause::Io(ref e)) => Some(e),
            Some(Cause::Hyper(ref e)) => Some(e),
            None => None
        }
    }
}

impl ::std::fmt::Display for Error {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
        f.write_str(self.description())
    }
}

pub mod catchers {
    use rocket::{self, Rocket, Request};
    use rocket_contrib::Json;
    use serde_json::Value;

    pub fn init_errors(rocket: Rocket) -> Rocket {
        rocket.catch(errors![not_found])
    }

    #[error(404)]
    fn not_found(_: &Request) -> Json<Value> {
        Json(json!({ "message": "Item not found" }))
    }
}