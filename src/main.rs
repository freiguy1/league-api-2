#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

extern crate rocket;
#[macro_use] extern crate rocket_contrib;
extern crate rocket_cors;
extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;
extern crate league_db;
extern crate diesel;
extern crate r2d2_diesel;
extern crate r2d2;
extern crate url;
extern crate pwhash;
extern crate chrono;
extern crate jwt;
extern crate crypto;
extern crate regex;
extern crate uuid;
extern crate rand;
extern crate hyper;
extern crate hyper_native_tls;
extern crate oauth2;

mod initialize;
mod guards;
mod v0_0_1;
mod error;
mod config;
mod auth;
mod response;
mod util;
mod stats;

fn main() {
    let config = config::Config::create();
    let mut server = rocket::ignite()
        .attach(initialize::init_cors(&config))
        .manage(initialize::init_pool(&config))
        .manage(initialize::init_cache(&config))
        .manage(config);
    server = error::catchers::init_errors(server);
    server = v0_0_1::init_routes(server);
    server = auth::init_routes(server);
    server.launch();
}
