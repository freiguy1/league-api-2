use url::Url;
use std::env::var;

const DEFAULT_DATABASE_URL: &'static str = "postgres://localhost/league_dev?user=league_dev_user&password=league_dev_user_password";
const DEFAULT_CACHE_EXPIRATION_MINUTES: i64 = 1440; // 1 day
const DEFAULT_BASE_URL: &'static str = "http://localhost:8000";
const DEFAULT_SECRET: &'static str = "fMrA3ILGLgJqkaEGgECuhQ34TziZEX";
const DEFAULT_TOKEN_EXPIRATION_MINUTES: u16 = 24*60; // 60 minutes * 24 = 24 hours
const DEFAULT_APP_NAME: &'static str = "League API";

pub struct Config {
    pub cache_expiration_minutes: i64,
    pub database_url: String,
    pub secret: String,
    pub app_name: String,
    pub token_expiration_minutes: u16,
    pub test_user: Option<TestUser>,
    pub email: Option<EmailConfig>,
    pub google_auth: Option<GoogleAuth>,
    pub base_url: Url,
    pub cors_domains: Vec<String>
}

impl Config {
    pub fn create() -> Config {
        Config {
            cache_expiration_minutes: var("CACHE_EXPIRATION_MINUTES").ok()
                .map(|i| i.parse().expect("unable to parse cache expiration var"))
                .unwrap_or(DEFAULT_CACHE_EXPIRATION_MINUTES),
            database_url: var("DATABASE_URL").ok().unwrap_or(DEFAULT_DATABASE_URL.to_string()),
            secret: var("SECRET").ok().unwrap_or(DEFAULT_SECRET.to_string()),
            app_name: var("APP_NAME").ok().unwrap_or(DEFAULT_APP_NAME.to_string()),
            token_expiration_minutes: var("TOKEN_EXPIRATION_MINUTES").ok()
                .map(|i| i.parse().expect("unable to parse token expiration from var"))
                .unwrap_or(DEFAULT_TOKEN_EXPIRATION_MINUTES),
            test_user: Self::get_test_user(),
            email: Self::get_email_config(),
            google_auth: Self::get_google_auth(),
            base_url: var("BASE_URL").ok()
                .map(|i| Url::parse(&i).expect("unable to parse base url from var"))
                .unwrap_or(Url::parse(DEFAULT_BASE_URL).unwrap()),
            cors_domains: Self::get_cors_domains()
        }
    }

    fn get_test_user() -> Option<TestUser> {
        let test_user_bearer_token = var("TEST_USER_BEARER_TOKEN");
        let test_user_email = var("TEST_USER_EMAIL");
        if let (Ok(token), Ok(email)) = (test_user_bearer_token, test_user_email) {
            Some(TestUser{
                bearer_token: token,
                email: email
            })
        } else { None }
    }

    fn get_email_config() -> Option<EmailConfig> {
        let email_username = var("EMAIL_USERNAME");
        let email_password = var("EMAIL_PASSWORD");
        let email_server = var("EMAIL_SERVER");
        let email_port = var("EMAIL_PORT");
        let email_from_email = var("EMAIL_FROM_EMAIL");
        let email_from_name = var("EMAIL_FROM_NAME");

        if email_username.is_ok() &&
            email_password.is_ok() &&
            email_server.is_ok() &&
            email_port.is_ok() &&
            email_from_email.is_ok() &&
            email_from_name.is_ok() {
            Some(EmailConfig {
                username: email_username.unwrap(),
                password: email_password.unwrap(),
                server: email_server.unwrap(),
                port: email_port.unwrap().parse().expect("couldn't parse email port from env var"),
                from_email: email_from_email.unwrap(),
                from_name: email_from_name.unwrap()
            })
        } else { None }
    }

    fn get_google_auth() -> Option<GoogleAuth> {
        let google_client_id = var("GOOGLE_CLIENT_ID");
        let google_client_secret = var("GOOGLE_CLIENT_SECRET");
        if let (Ok(id), Ok(secret)) = (google_client_id, google_client_secret) {
            Some(GoogleAuth {
                client_id: id,
                client_secret: secret
            })
        } else { None }
    }

    fn get_cors_domains() -> Vec<String> {
        if let Ok(string_list) = var("CORS_DOMAINS") {
            string_list.split(',').into_iter().map(|d| d.to_string()).collect()
        } else { vec!["http://elm-lang.org".to_string()] }
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct GoogleAuth {
    pub client_id: String,
    pub client_secret: String
}

#[derive(Deserialize, Debug, Clone)]
pub struct TestUser {
    email: String,
    bearer_token: String
}

#[derive(Deserialize, Debug, Clone)]
pub struct EmailConfig {
    username: String,
    password: String,
    server: String,
    port: u16,
    from_email: String,
    from_name: String
}
