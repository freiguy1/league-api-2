use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel::expression::dsl::*;
use schema::team;
use error::Error;
use std::collections::{ HashSet, HashMap };

#[derive(Queryable, AsChangeset)]
#[table_name="team"]
pub struct TeamDao {
    pub id: i32,
    pub sequence: i32,
    pub league_id: i32,
    pub name: String,
    pub captain_name: String,
    pub captain_email: String,
    pub captain_phone: String
}

#[derive(Insertable)]
#[table_name="team"]
pub struct NewTeam {
    pub sequence: i32,
    pub league_id: i32,
    pub name: String,
    pub captain_name: String,
    pub captain_email: String,
    pub captain_phone: String
}

pub struct TeamRepo<'a> {
    conn: &'a PgConnection
}

impl <'a> TeamRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> TeamRepo {
        TeamRepo {
            conn: conn
        }
    }
    
    pub fn create(&self, new_team: &NewTeam) -> Result<TeamDao, Error> {
        let result = ::diesel::insert_into(team::table).values(new_team)
            .get_result(self.conn)?;
        Ok(result)
    }

    pub fn get_by_id(&self, team_id: i32) -> Result<Option<TeamDao>, Error> {
        let result = try!(team::table
            .filter(team::id.eq(team_id))
            .first(self.conn)
            .optional());
        Ok(result)
    }

    pub fn get_by_league_id(&self, league_id: i32) -> Result<Vec<TeamDao>, Error> {
        let result = try!(team::table.filter(team::league_id.eq(league_id))
            .load(self.conn));
        Ok(result)
    }

    pub fn get_by_league_ids(&self, league_ids: &HashSet<i32>) -> Result<Vec<TeamDao>, Error> {
        if league_ids.len() == 0 {
            return Ok(Vec::new());
        }
        let result = try!(team::table.filter(team::league_id.eq_any(league_ids))
            .load(self.conn));
        Ok(result)
    }

    pub fn update(&self, updated_team: &TeamDao) -> Result<TeamDao, Error> {
        let result = try!(::diesel::update(team::table.filter(team::id.eq(updated_team.id)))
            .set(updated_team)
            .get_result(self.conn));
        Ok(result)
    }

    pub fn delete(&self, id: i32) -> Result<usize, Error> {
        let result = try!(::diesel::delete(team::table.filter(team::id.eq(id)))
            .execute(self.conn));
        Ok(result)
    }

    pub fn get_count_by_league_ids(&self, league_ids: &HashSet<i32>) -> Result<HashMap<i32, i32>, Error> {
        if league_ids.len() == 0 { return Ok(HashMap::new()); }

        let league_ids_str: Vec<String> = league_ids.iter().map(|lid| lid.to_string()).collect();
        let query = format!("SELECT league_id, COUNT(id) FROM team WHERE league_id in ({}) GROUP BY league_id", league_ids_str.join(", "));
        let result = try!(sql(&query).load::<(i32, i64)>(self.conn));
        let mut counts = HashMap::new();
        for league_id in league_ids.iter() {
            if let Some(tup) = result.iter().find(|r| r.0 == *league_id) {
                counts.insert(tup.0, tup.1 as i32);
            } else {
                counts.insert(*league_id, 0);
            }
        }
        Ok(counts)
    }
}

/*
#[test]
fn create_delete_team_test() {
    let connection = ::establish_connection();
    let league_repo = ::repo::LeagueRepo::new(&connection);
    let team_repo = TeamRepo::new(&connection);

    let new_league = ::repo::NewLeague {
        name: "My new test league".to_string(),
        information: Some("This is the information for the test league! Sweet, eh".to_string()),
        contact_email: "league.manager@league.man".to_string(),
        contact_phone: "1234567890".to_string(),
        sport_id: 6
    };

    let created_league = league_repo.create(&new_league);

    let new_team = NewTeam {
        number: 1,
        league_id: created_league.id,
        name: "First Team!".to_string(),
        captain_name: "Captain Name".to_string(),
        captain_email: "Captain Email".to_string(),
        captain_phone: "1234567890".to_string()
    };

    let created_team = team_repo.create(&new_team);

    assert_eq!(created_team.name, new_team.name);

    team_repo.delete(created_team.id);
    league_repo.delete(created_league.id);
}
*/
