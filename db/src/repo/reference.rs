use diesel::pg::PgConnection;
use diesel::prelude::*;
use schema::{sport, matchup_status};
use error::Error;

#[derive(Queryable)]
pub struct Sport {
    pub id: i32,
    pub name: String
}

#[derive(Queryable)]
pub struct MatchupStatus {
    pub id: i32,
    pub name: String
}

pub struct ReferenceRepo<'a> {
    conn: &'a PgConnection
}

impl<'a> ReferenceRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> ReferenceRepo {
        ReferenceRepo {
            conn: conn
        }
    }

    pub fn get_sports(&self) -> Result<Vec<Sport>, Error> {
        Ok(try!(sport::table.load(self.conn)))
    }
    
    pub fn get_matchup_statuses(&self) -> Result<Vec<MatchupStatus>, Error> {
        Ok(try!(matchup_status::table.load(self.conn)))
    }
}

