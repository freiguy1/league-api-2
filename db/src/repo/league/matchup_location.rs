use diesel::pg::PgConnection;
use diesel::prelude::*;
use schema::matchup_location;
use error::Error;

#[derive(Queryable, AsChangeset)]
#[table_name="matchup_location"]
pub struct MatchupLocationDao {
    pub id: i32,
    pub name: String,
    pub league_id: i32
}

#[derive(Insertable)]
#[table_name="matchup_location"]
pub struct NewMatchupLocation {
    pub name: String,
    pub league_id: i32
}

pub struct MatchupLocationRepo<'a> {
    conn: &'a PgConnection
}

impl <'a> MatchupLocationRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> MatchupLocationRepo {
        MatchupLocationRepo {
            conn: conn
        }
    }
    
    pub fn create_many(&self, new_matchup_locations: &Vec<NewMatchupLocation>) -> Result<(), Error> {
        if new_matchup_locations.len() == 0 {
            return Ok(());
        }
        ::diesel::insert_into(matchup_location::table).values(new_matchup_locations).execute(self.conn)?;
        Ok(())
    }

    pub fn get_by_league_id(&self, league_id: i32) -> Result<Vec<MatchupLocationDao>, Error> {
        let result = try!(matchup_location::table.filter(matchup_location::league_id.eq(league_id))
            .load(self.conn));
        Ok(result)
    }

    pub fn get_by_league_ids(&self, league_ids: &Vec<i32>) -> Result<Vec<MatchupLocationDao>, Error> {
        let result = try!(matchup_location::table.filter(matchup_location::league_id.eq_any(league_ids.iter()))
            .load(self.conn));
        Ok(result)
    }


    pub fn update(&self, updated_matchup_location: &MatchupLocationDao) -> Result<MatchupLocationDao, Error> {
        let filter = matchup_location::table.filter(matchup_location::id.eq(updated_matchup_location.id));
        let result = try!(
            ::diesel::update(filter)
            .set(updated_matchup_location)
            .get_result(self.conn));
        Ok(result)
    }

    pub fn delete_many(&self, ids: &Vec<i32>) -> Result<usize, Error> {
        if ids.len() == 0 { return Ok(0); }
        let filter = matchup_location::table.filter(matchup_location::id.eq_any(ids.iter()));
        let result = try!(::diesel::delete(filter).execute(self.conn));
        Ok(result)
    }

    pub fn delete_by_league_id(&self, league_id: i32) -> Result<(), Error> {
        let filter = matchup_location::table.filter(matchup_location::league_id.eq(league_id));
        try!(::diesel::delete(filter).execute(self.conn));
        Ok(())
    }
}
