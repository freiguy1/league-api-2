use diesel::pg::PgConnection;
use diesel::prelude::*;
use schema::league_administrator;
use error::Error;

#[derive(Queryable, AsChangeset)]
#[table_name="league_administrator"]
pub struct LeagueAdministratorDao {
    pub id: i32,
    pub league_id: i32,
    pub user_id: i32
}

#[derive(Insertable)]
#[table_name="league_administrator"]
pub struct NewLeagueAdministrator {
    pub league_id: i32,
    pub user_id: i32
}

pub struct LeagueAdministratorRepo<'a> {
    conn: &'a PgConnection
}

impl <'a> LeagueAdministratorRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> LeagueAdministratorRepo {
        LeagueAdministratorRepo {
            conn: conn
        }
    }

    pub fn create_many(&self, new_league_administrators: &Vec<NewLeagueAdministrator>) -> Result<(), Error> {
        if new_league_administrators.len() == 0 {
            return Ok(());
        }
        ::diesel::insert_into(league_administrator::table)
            .values(new_league_administrators)
            .execute(self.conn)?;
        Ok(())
    }

    //pub fn create(&self, new_league_administrator: &NewLeagueAdministrator) -> Result<LeagueAdministratorDao, Error> {
        //let result = try!(::diesel::insert(new_league_administrator).into(league_administrator::table)
            //.get_result(self.conn));
        //Ok(result)
    //}

    pub fn get_by_league_id(&self, league_id: i32) -> Result<Vec<LeagueAdministratorDao>, Error> {
        let result = try!(league_administrator::table.filter(league_administrator::league_id.eq(league_id))
            .load(self.conn));
        Ok(result)
    }

    pub fn get_by_user_id(&self, user_id: i32) -> Result<Vec<LeagueAdministratorDao>, Error> {
        let result = try!(league_administrator::table.filter(league_administrator::user_id.eq(user_id))
            .load(self.conn));
        Ok(result)
    }

    pub fn get_by_league_ids(&self, league_ids: &Vec<i32>) -> Result<Vec<LeagueAdministratorDao>, Error> {
        let result = try!(league_administrator::table.filter(league_administrator::league_id.eq_any(league_ids.iter()))
            .load(self.conn));
        Ok(result)
    }

    pub fn update(&self, updated_league_administrator: &LeagueAdministratorDao) -> Result<LeagueAdministratorDao, Error> {
        let result = try!(::diesel::update(league_administrator::table.filter(league_administrator::id.eq(updated_league_administrator.id)))
            .set(updated_league_administrator)
            .get_result(self.conn));
        Ok(result)
    }

    pub fn delete_many(&self, ids: &Vec<i32>) -> Result<usize, Error> {
        if ids.len() == 0 { return Ok(0); }
        let filter = league_administrator::table.filter(league_administrator::id.eq_any(ids.iter()));
        let result = try!(::diesel::delete(filter).execute(self.conn));
        Ok(result)
    }

    pub fn delete_by_league_id(&self, league_id: i32) -> Result<(), Error> {
        try!(::diesel::delete(league_administrator::table.filter(league_administrator::league_id.eq(league_id)))
            .execute(self.conn));
        Ok(())
    }
}
