// mod matchup;

use chrono::NaiveDateTime;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use schema::{matchup, team_event, bye, game};
use error::Error;
use diesel::expression::dsl::*;
use std::collections::HashSet;

pub enum TeamEvent {
    Matchup(Matchup),
    Bye(Bye)
}

impl TeamEvent {
    pub fn id(&self) -> i32 {
        match *self {
            TeamEvent::Matchup(ref g) => g.id,
            TeamEvent::Bye(ref b) => b.id
        }
    }

    pub fn is_matchup(&self) -> bool {
        match *self {
            TeamEvent::Matchup(_) => true,
            TeamEvent::Bye(_) => false
        }
    }

    pub fn is_bye(&self) -> bool {
        !self.is_matchup()
    }

    pub fn matchup(self) -> Option<Matchup> {
        match self {
            TeamEvent::Matchup(g) => Some(g),
            TeamEvent::Bye(_) => None
        }
    }

    pub fn bye(self) -> Option<Bye> {
        match self {
            TeamEvent::Matchup(_) => None,
            TeamEvent::Bye(b) => Some(b)
        }
    }

    pub fn matchup_ref(&self) -> Option<&Matchup> {
        match self {
            &TeamEvent::Matchup(ref g) => Some(g),
            &TeamEvent::Bye(_) => None
        }
    }

    pub fn bye_ref(&self) -> Option<&Bye> {
        match self {
            &TeamEvent::Matchup(_) => None,
            &TeamEvent::Bye(ref b) => Some(b)
        }
    }

    pub fn league_id(&self) -> i32 {
        match *self {
            TeamEvent::Matchup(ref g) => g.league_id,
            TeamEvent::Bye(ref b) => b.league_id
        }
    }

    pub fn notes(&self) -> Option<&String> {
        match *self {
            TeamEvent::Matchup(ref g) => g.notes.as_ref(),
            TeamEvent::Bye(ref b) => b.notes.as_ref()
        }
    }

    pub fn datetime(&self) -> &NaiveDateTime {
        match *self {
            TeamEvent::Matchup(ref g) => &g.datetime,
            TeamEvent::Bye(ref b) => &b.datetime
        }
    }

    pub fn team_ids(&self) -> Vec<i32> {
        match *self {
            TeamEvent::Matchup(ref g) => vec![g.home_team_id, g.away_team_id],
            TeamEvent::Bye(ref b) => vec![b.team_id]
        }
    }

    fn new_matchup(matchup_dao: &MatchupDao, team_event_dao: &TeamEventDao, game_daos: &Vec<GameDao>) -> TeamEvent {
        let games = game_daos.iter().map(|g| Game {
            id: g.id,
            home_team_score: g.home_team_score,
            away_team_score: g.away_team_score,
            sequence: g.sequence,
            notes: g.notes.clone()
        }).collect();

        TeamEvent::Matchup(Matchup{
            id: team_event_dao.id,
            // home_team_score: matchup_dao.home_team_score,
            // away_team_score: matchup_dao.away_team_score,
            datetime: team_event_dao.datetime.clone(),
            matchup_location_id: matchup_dao.matchup_location_id,
            league_id: team_event_dao.league_id,
            home_team_id: matchup_dao.home_team_id,
            away_team_id: matchup_dao.away_team_id,
            matchup_status_id: matchup_dao.matchup_status_id,
            rescheduled_team_event_id: matchup_dao.rescheduled_team_event_id,
            notes: team_event_dao.notes.clone(),
            games: games
        })
    }

    fn new_bye(bye_dao: &ByeDao, team_event_dao: &TeamEventDao) -> TeamEvent {
        TeamEvent::Bye(Bye{
            id: team_event_dao.id,
            datetime: team_event_dao.datetime.clone(),
            league_id: team_event_dao.league_id,
            team_id: bye_dao.team_id,
            notes: team_event_dao.notes.clone()
        })
    }
}

pub struct Matchup {
    pub id: i32,
    pub datetime: NaiveDateTime,
    pub matchup_location_id: i32,
    pub league_id: i32,
    pub home_team_id: i32,
    pub away_team_id: i32,
    pub matchup_status_id: i32,
    pub rescheduled_team_event_id: Option<i32>,
    pub notes: Option<String>,
    pub games: Vec<Game>
}

pub struct Game {
    pub id: i32,
    pub home_team_score: Option<i32>,
    pub away_team_score: Option<i32>,
    pub sequence: i32,
    pub notes: Option<String>
}

pub struct Bye {
    pub id: i32,
    pub datetime: NaiveDateTime,
    pub league_id: i32,
    pub team_id: i32,
    pub notes: Option<String>
}

pub struct UpdateMatchup {
    pub id: i32,
    pub datetime: NaiveDateTime,
    pub matchup_location_id: i32,
    pub league_id: i32,
    pub home_team_id: i32,
    pub away_team_id: i32,
    pub matchup_status_id: i32,
    pub rescheduled_team_event_id: Option<i32>,
    pub notes: Option<String>,
    pub new_games: Vec<NewGame>,
    pub update_games: Vec<Game>,
    pub delete_games: HashSet<i32>
}

pub struct NewGame {
    pub home_team_score: Option<i32>,
    pub away_team_score: Option<i32>,
    pub sequence: i32,
    pub notes: Option<String>
}

pub struct NewMatchup {
    pub datetime: NaiveDateTime,
    pub matchup_location_id: i32,
    pub league_id: i32,
    pub home_team_id: i32,
    pub away_team_id: i32,
    pub matchup_status_id: i32,
    pub rescheduled_team_event_id: Option<i32>,
    pub notes: Option<String>,
    pub games: Vec<NewGame>
}

pub struct NewBye {
    pub datetime: NaiveDateTime,
    pub league_id: i32,
    pub team_id: i32,
    pub notes: Option<String>
}

pub struct TeamEventRepo<'a> {
    conn: &'a PgConnection
}

impl <'a> TeamEventRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> TeamEventRepo {
        TeamEventRepo {
            conn: conn
        }
    }
    
    pub fn create_matchup(&self, new_matchup: &NewMatchup) -> Result<TeamEvent, Error> {
        let new_event_dao = NewTeamEventDao {
            league_id: new_matchup.league_id,
            datetime: new_matchup.datetime.clone(),
            notes: new_matchup.notes.clone()
        };
        let event_result: TeamEventDao = ::diesel::insert_into(team_event::table)
            .values(&new_event_dao).get_result(self.conn)?;
        let matchup_dao = MatchupDao {
            team_event_id: event_result.id,
            matchup_location_id: new_matchup.matchup_location_id,
            home_team_id: new_matchup.home_team_id,
            away_team_id: new_matchup.away_team_id,
            matchup_status_id: new_matchup.matchup_status_id,
            rescheduled_team_event_id: new_matchup.rescheduled_team_event_id
        };
        let matchup_result: MatchupDao = ::diesel::insert_into(matchup::table)
            .values(&matchup_dao).get_result(self.conn)?;

        let mut game_results: Vec<GameDao> = Vec::new();
        for new_game in new_matchup.games.iter() {
            let new_game_dao = NewGameDao {
                sequence: new_game.sequence,
                team_event_id: event_result.id,
                home_team_score: new_game.home_team_score.clone(),
                away_team_score: new_game.away_team_score.clone(),
                notes: new_game.notes.clone()
            };
            game_results.push(::diesel::insert_into(game::table).values(&new_game_dao).get_result(self.conn)?);
        }

        Ok(TeamEvent::new_matchup(&matchup_result, &event_result, &game_results))
    }

    pub fn create_bye(&self, new_bye: &NewBye) -> Result<TeamEvent, Error> {
        let new_event_dao = NewTeamEventDao {
            league_id: new_bye.league_id,
            datetime: new_bye.datetime.clone(),
            notes: new_bye.notes.clone()
        };
        let event_result: TeamEventDao = ::diesel::insert_into(team_event::table)
            .values(&new_event_dao).get_result(self.conn)?;
        let bye_dao = ByeDao {
            team_event_id: event_result.id,
            team_id: new_bye.team_id
        };
        let bye_result: ByeDao = ::diesel::insert_into(bye::table).values(&bye_dao).get_result(self.conn)?;
        Ok(TeamEvent::new_bye(&bye_result, &event_result))
    }

    pub fn get_single(&self, team_event_id: i32) -> Result<Option<TeamEvent>, Error> {
        let team_event_result = try!(team_event::table
            .filter(team_event::id.eq(team_event_id))
            .first(self.conn).optional());
        let team_event_result = match team_event_result {
            Some(ter) => ter,
            None => return Ok(None)
        };
        let matchup_result = try!(matchup::table
            .filter(matchup::team_event_id.eq(team_event_id))
            .first(self.conn).optional());
        if let Some(matchup_result) = matchup_result {
            let game_results = try!(game::table.filter(game::team_event_id.eq(team_event_id))
                                    .load(self.conn));
            return Ok(Some(TeamEvent::new_matchup(&matchup_result, &team_event_result, &game_results)));
        }
        let bye_result = try!(bye::table
            .filter(bye::team_event_id.eq(team_event_id))
            .first(self.conn).optional());
        if let Some(bye_result) = bye_result {
            Ok(Some(TeamEvent::new_bye(&bye_result, &team_event_result)))
        } else { Ok(None) }
    }

    pub fn get_by_league_ids(&self, league_ids: &Vec<i32>) -> Result<Vec<TeamEvent>, Error> {
        if league_ids.len() == 0 {
            return Ok(Vec::new());
        }
        let team_events: Vec<TeamEventDao> = try!(team_event::table.filter(team_event::league_id.eq(any(league_ids))).load(self.conn));
        let team_event_ids = team_events.iter().map(|te| te.id).collect::<Vec<_>>();
        let matchups: Vec<MatchupDao> = try!(matchup::table.filter(matchup::team_event_id.eq(any(&team_event_ids))).load(self.conn));
        let byes: Vec<ByeDao> = try!(bye::table.filter(bye::team_event_id.eq(any(&team_event_ids))).load(self.conn));
        let mut games: Vec<GameDao> = try!(game::table.filter(game::team_event_id.eq(any(&team_event_ids))).load(self.conn));

        let mut result = Vec::with_capacity(team_events.len());
        for team_event in team_events.iter() {
            if let Some(matchup) = matchups.iter().find(|g| g.team_event_id == team_event.id) {
                let (matchup_games, others) = games.into_iter().partition(|g| g.team_event_id == team_event.id);
                games = others;
                result.push(TeamEvent::new_matchup(matchup, team_event, &matchup_games));
            } else if let Some(bye) = byes.iter().find(|b| b.team_event_id == team_event.id) {
                result.push(TeamEvent::new_bye(bye, team_event));
            }
        }
        Ok(result)
    }

    pub fn get_many(&self, team_event_ids: &Vec<i32>) -> Result<Vec<TeamEvent>, Error> {
        let team_events: Vec<TeamEventDao> = try!(team_event::table.filter(team_event::id.eq(any(team_event_ids))).load(self.conn));
        let matchups: Vec<MatchupDao> = try!(matchup::table.filter(matchup::team_event_id.eq(any(team_event_ids))).load(self.conn));
        let byes: Vec<ByeDao> = try!(bye::table.filter(bye::team_event_id.eq(any(team_event_ids))).load(self.conn));
        let mut games: Vec<GameDao> = try!(game::table.filter(game::team_event_id.eq(any(team_event_ids))).load(self.conn));


        let mut result = Vec::with_capacity(team_events.len());
        for team_event in team_events.iter() {
            if let Some(matchup) = matchups.iter().find(|g| g.team_event_id == team_event.id) {
                let (matchup_games, others) = games.into_iter().partition(|g| g.team_event_id == team_event.id);
                games = others;
                result.push(TeamEvent::new_matchup(matchup, team_event, &matchup_games));
            } else if let Some(bye) = byes.iter().find(|b| b.team_event_id == team_event.id) {
                result.push(TeamEvent::new_bye(bye, team_event));
            }
        }
        Ok(result)
    }

    pub fn get_by_team_ids(&self, team_ids: &HashSet<i32>) -> Result<Vec<TeamEvent>, Error> {
        let matchups: Vec<MatchupDao> = try!(matchup::table
            .filter(matchup::home_team_id .eq_any(team_ids)
                .or(matchup::away_team_id.eq_any(team_ids)))
            .load(self.conn));
        let byes: Vec<ByeDao> = try!(bye::table
            .filter(bye::team_id.eq_any(team_ids))
            .load(self.conn));
        let team_event_ids: Vec<i32> = matchups.iter().map(|g| g.team_event_id).chain(byes.iter().map(|b| b.team_event_id)).collect();

        let team_events: Vec<TeamEventDao> = try!(team_event::table.filter(team_event::id.eq(any(&team_event_ids))).load(self.conn));
        let mut games: Vec<GameDao> = try!(game::table.filter(game::team_event_id.eq(any(team_event_ids))).load(self.conn));

        let mut result = Vec::with_capacity(team_events.len());
        for team_event in team_events.iter() {
            if let Some(matchup) = matchups.iter().find(|g| g.team_event_id == team_event.id) {
                let (matchup_games, others) = games.into_iter().partition(|g| g.team_event_id == team_event.id);
                games = others;
                result.push(TeamEvent::new_matchup(matchup, team_event, &matchup_games));
            } else if let Some(bye) = byes.iter().find(|b| b.team_event_id == team_event.id) {
                result.push(TeamEvent::new_bye(bye, team_event));
            }
        }
        Ok(result)
    }

    pub fn update_matchup(&self, updated_matchup: &UpdateMatchup) -> Result<(), Error> {
        // Update TeamEvent
        let team_event_dao = TeamEventDao {
            id: updated_matchup.id,
            league_id: updated_matchup.league_id,
            datetime: updated_matchup.datetime.clone(),
            notes: updated_matchup.notes.clone()
        };
        try!(::diesel::update(team_event::table.filter(team_event::id.eq(updated_matchup.id)))
            .set(&team_event_dao)
            .execute(self.conn));

        // Update Matchup
        let update_dao = MatchupDao {
            team_event_id: updated_matchup.id,
            // home_team_score: g.home_team_score,
            // away_team_score: g.away_team_score,
            matchup_location_id: updated_matchup.matchup_location_id,
            home_team_id: updated_matchup.home_team_id,
            away_team_id: updated_matchup.away_team_id,
            matchup_status_id: updated_matchup.matchup_status_id,
            rescheduled_team_event_id: updated_matchup.rescheduled_team_event_id
        };
        try!(::diesel::update(matchup::table.filter(matchup::team_event_id.eq(update_dao.team_event_id)))
             .set(&update_dao).execute(self.conn));

        // Delete games
        for delete_game_id in updated_matchup.delete_games.iter() {
            try!(::diesel::delete(game::table.filter(game::id.eq(delete_game_id)))
                .execute(self.conn));
        }

        // Update games
        for updated_game in updated_matchup.update_games.iter() {
            let update_game_dao = GameDao {
                id: updated_game.id,
                sequence: updated_game.sequence,
                team_event_id: updated_matchup.id,
                home_team_score: updated_game.home_team_score,
                away_team_score: updated_game.away_team_score,
                notes: updated_game.notes.clone()
            };
        try!(::diesel::update(game::table.filter(game::id.eq(updated_game.id)))
             .set(&update_game_dao).execute(self.conn));
        }

        // Add games
        for new_game in updated_matchup.new_games.iter() {
            let new_game_dao = NewGameDao {
                sequence: new_game.sequence,
                team_event_id: updated_matchup.id,
                home_team_score: new_game.home_team_score.clone(),
                away_team_score: new_game.away_team_score.clone(),
                notes: new_game.notes.clone()
            };
            ::diesel::insert_into(game::table).values(&new_game_dao).execute(self.conn)?;
        }

        Ok(())
    }

    pub fn update_bye(&self, updated_bye: &Bye) -> Result<(), Error> {
        // Update team event
        let team_event_dao = TeamEventDao {
            id: updated_bye.id,
            league_id: updated_bye.league_id,
            datetime: updated_bye.datetime.clone(),
            notes: updated_bye.notes.clone()
        };
        try!(::diesel::update(team_event::table.filter(team_event::id.eq(updated_bye.id)))
            .set(&team_event_dao)
            .execute(self.conn));

        // Update bye
        let bye_dao: ByeDao = ByeDao {
            team_event_id: updated_bye.id,
            team_id: updated_bye.team_id
        };
        try!(::diesel::update(bye::table.filter(bye::team_event_id.eq(updated_bye.id)))
            .set(&bye_dao)
            .execute(self.conn));
        Ok(())
    }

    pub fn update(&self, updated_team_event: &TeamEvent) -> Result<(), Error> {
        match *updated_team_event {
            TeamEvent::Matchup(ref g) => {
                let matchup_dao = MatchupDao {
                    team_event_id: updated_team_event.id(),
                    // home_team_score: g.home_team_score,
                    // away_team_score: g.away_team_score,
                    matchup_location_id: g.matchup_location_id,
                    home_team_id: g.home_team_id,
                    away_team_id: g.away_team_id,
                    matchup_status_id: g.matchup_status_id,
                    rescheduled_team_event_id: g.rescheduled_team_event_id
                };
                try!(::diesel::update(matchup::table.filter(matchup::team_event_id.eq(updated_team_event.id())))
                    .set(&matchup_dao)
                    .execute(self.conn));
            },
            TeamEvent::Bye(ref b) => {
                let bye_dao: ByeDao = ByeDao {
                    team_event_id: updated_team_event.id(),
                    team_id: b.team_id
                };
                try!(::diesel::update(bye::table.filter(bye::team_event_id.eq(updated_team_event.id())))
                    .set(&bye_dao)
                    .execute(self.conn));
            }
        };
        let team_event_dao = TeamEventDao {
            id: updated_team_event.id(),
            league_id: updated_team_event.league_id(),
            datetime: updated_team_event.datetime().clone(),
            notes: updated_team_event.notes().map(|n| n.clone())
        };
        try!(::diesel::update(team_event::table.filter(team_event::id.eq(updated_team_event.id())))
            .set(&team_event_dao)
            .execute(self.conn));
        Ok(())
    }

    pub fn delete_many(&self, ids: &Vec<i32>) -> Result<usize, Error> {
        if ids.len() == 0 {
            return Ok(0);
        }
        try!(::diesel::delete(game::table.filter(game::team_event_id.eq_any(ids)))
            .execute(self.conn));
        try!(::diesel::delete(matchup::table.filter(matchup::team_event_id.eq_any(ids)))
            .execute(self.conn));
        try!(::diesel::delete(bye::table.filter(bye::team_event_id.eq_any(ids)))
            .execute(self.conn));
        let result = try!(::diesel::delete(team_event::table.filter(team_event::id.eq_any(ids)))
            .execute(self.conn));
        Ok(result)
    }
}

#[derive(Queryable, AsChangeset)]
#[table_name="team_event"]
#[changeset_options(treat_none_as_null = "true")]
struct TeamEventDao {
    id: i32,
    datetime: NaiveDateTime,
    league_id: i32,
    notes: Option<String>
}

#[derive(Insertable)]
#[table_name="team_event"]
struct NewTeamEventDao {
    league_id: i32,
    datetime: NaiveDateTime,
    notes: Option<String>
}

#[derive(Queryable, AsChangeset)]
#[table_name="game"]
#[changeset_options(treat_none_as_null = "true")]
struct GameDao {
    id: i32,
    sequence: i32,
    team_event_id: i32,
    home_team_score: Option<i32>,
    away_team_score: Option<i32>,
    notes: Option<String>
}

#[derive(Insertable)]
#[table_name="game"]
struct NewGameDao {
    sequence: i32,
    team_event_id: i32,
    home_team_score: Option<i32>,
    away_team_score: Option<i32>,
    notes: Option<String>
}

#[derive(Queryable, AsChangeset, Insertable)]
#[table_name="matchup"]
#[changeset_options(treat_none_as_null = "true")]
struct MatchupDao {
    team_event_id: i32,
    matchup_location_id: i32,
    home_team_id: i32,
    away_team_id: i32,
    matchup_status_id: i32,
    rescheduled_team_event_id: Option<i32>
}

#[derive(Queryable, AsChangeset, Insertable)]
#[table_name="bye"]
struct ByeDao {
    team_event_id: i32,
    team_id: i32
}
