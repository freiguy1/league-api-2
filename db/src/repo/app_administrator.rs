use diesel::pg::PgConnection;
use diesel::prelude::*;
use schema::app_administrator;
use error::Error;


#[derive(Queryable)]
pub struct AppAdministratorDao {
    pub id: i32,
    pub user_id: i32
}


#[derive(Insertable)]
#[table_name="app_administrator"]
struct NewAppAdministrator {
    pub user_id: i32
}

pub struct AppAdministratorRepo<'a> {
    conn: &'a PgConnection
}

impl <'a> AppAdministratorRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> AppAdministratorRepo {
        AppAdministratorRepo {
            conn: conn
        }
    }
    
    pub fn create(&self, user_id: i32) -> Result<AppAdministratorDao, Error> {
        let new_app_admin = NewAppAdministrator { user_id: user_id };
        let result = ::diesel::insert_into(app_administrator::table)
            .values(&new_app_admin)
            .get_result(self.conn)?;
        Ok(result)
    }

    pub fn get_by_user_id(&self, user_id: i32) -> Result<Option<AppAdministratorDao>, Error> {
        let result = try!(app_administrator::table.filter(app_administrator::user_id.eq(user_id))
            .first(self.conn)
            .optional());
        Ok(result)
    }
}
