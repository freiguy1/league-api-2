use diesel::pg::PgConnection;
use diesel::prelude::*;
use schema::log;
use error::Error;
use chrono::NaiveDateTime;

#[derive(Queryable)]
pub struct LogDao {
    pub id: i32,
    pub context: String,
    pub description: String,
    pub datetime: NaiveDateTime,
    pub severity: i32,
    pub http_verb: Option<String>,
    pub request_url: Option<String>,
    pub authenticated_user_id: Option<i32>
}

#[derive(Insertable)]
#[table_name="log"]
pub struct NewLog {
    pub context: String,
    pub description: String,
    pub datetime: NaiveDateTime,
    pub severity: i32,
    pub http_verb: Option<String>,
    pub request_url: Option<String>,
    pub authenticated_user_id: Option<i32>
}

pub struct LogRepo<'a> {
    conn: &'a PgConnection
}

impl<'a> LogRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> LogRepo {
        LogRepo {
            conn: conn
        }
    }

    pub fn create(&self, new_log: &NewLog) -> Result<LogDao, Error> {
        let result = ::diesel::insert_into(log::table).values(new_log)
            .get_result(self.conn)?;
        Ok(result)
    }

    pub fn get_in_span(&self, early: &Option<NaiveDateTime>, late: &Option<NaiveDateTime>) -> Result<Vec<LogDao>, Error> {
        let result = if early.is_some() && late.is_some() {
            let filter = log::datetime.gt(early.as_ref().unwrap())
                .and(log::datetime.lt(late.as_ref().unwrap()));
            try!(log::table.filter(filter).load(self.conn))
        } else if early.is_some() {
            let filter = log::datetime.gt(early.as_ref().unwrap());
            try!(log::table.filter(filter).load(self.conn))
        } else if late.is_some() {
            let filter = log::datetime.lt(late.as_ref().unwrap());
            try!(log::table.filter(filter).load(self.conn))
        } else {
            try!(log::table.load(self.conn))
        };
        Ok(result)
    }
}
