use schema::league;

#[derive(Queryable)]
pub struct League {
    pub id: i32,
    pub name: String,
    pub information: Option<String>,
    pub contact_email: String,
    pub contact_phone: String,
    pub sport_id: i32
}


#[derive(Queryable)]
pub struct Sport {
    pub id: i32,
    pub name: String
}

#[derive(Queryable)]
pub struct Team {
    pub id: i32,
    pub name: String,
    pub captain_name: String,
    pub captain_email: String,
    pub captain_phone: String
}

#[insertable_into(league)]
pub struct NewLeague {
    pub name: String,
    pub information: Option<String>,
    pub contact_email: String,
    pub contact_phone: String,
    pub sport_id: i32
}
