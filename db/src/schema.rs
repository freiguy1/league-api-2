table! {
    app_administrator (id) {
        id -> Int4,
        user_id -> Int4,
    }
}

table! {
    bye (team_event_id) {
        team_event_id -> Int4,
        team_id -> Int4,
    }
}

table! {
    game (id) {
        id -> Int4,
        sequence -> Int4,
        team_event_id -> Int4,
        home_team_score -> Nullable<Int4>,
        away_team_score -> Nullable<Int4>,
        notes -> Nullable<Text>,
    }
}

table! {
    league (id) {
        id -> Int4,
        name -> Varchar,
        information -> Nullable<Text>,
        sport_id -> Int4,
        organization -> Varchar,
        contact_phone -> Varchar,
        games_per_matchup -> Int4,
    }
}

table! {
    league_administrator (id) {
        id -> Int4,
        league_id -> Int4,
        user_id -> Int4,
    }
}

table! {
    league_user (id) {
        id -> Int4,
        first_name -> Varchar,
        last_name -> Varchar,
        phone -> Varchar,
        email -> Varchar,
        user_password_id -> Nullable<Int4>,
        is_confirmed -> Bool,
        confirmation_key -> Varchar,
        created_datetime -> Timestamp,
    }
}

table! {
    log (id) {
        id -> Int4,
        context -> Varchar,
        description -> Varchar,
        datetime -> Timestamp,
        severity -> Int4,
        http_verb -> Nullable<Varchar>,
        request_url -> Nullable<Varchar>,
        authenticated_user_id -> Nullable<Int4>,
    }
}

table! {
    matchup (team_event_id) {
        team_event_id -> Int4,
        matchup_location_id -> Int4,
        home_team_id -> Int4,
        away_team_id -> Int4,
        matchup_status_id -> Int4,
        rescheduled_team_event_id -> Nullable<Int4>,
    }
}

table! {
    matchup_location (id) {
        id -> Int4,
        name -> Varchar,
        league_id -> Int4,
    }
}

table! {
    matchup_status (id) {
        id -> Int4,
        name -> Varchar,
    }
}

table! {
    oauth2_info (id) {
        id -> Int4,
        provider -> Varchar,
        profile_id -> Varchar,
        email -> Varchar,
        user_id -> Nullable<Int4>,
    }
}

table! {
    sport (id) {
        id -> Int4,
        name -> Varchar,
    }
}

table! {
    team (id) {
        id -> Int4,
        sequence -> Int4,
        league_id -> Int4,
        name -> Varchar,
        captain_name -> Varchar,
        captain_email -> Varchar,
        captain_phone -> Varchar,
    }
}

table! {
    team_event (id) {
        id -> Int4,
        datetime -> Timestamp,
        league_id -> Int4,
        notes -> Nullable<Text>,
    }
}

table! {
    team_follower (id) {
        id -> Int4,
        team_id -> Int4,
        user_id -> Int4,
    }
}

table! {
    user_password (id) {
        id -> Int4,
        password -> Varchar,
        salt -> Varchar,
    }
}
