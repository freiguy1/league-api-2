CREATE TABLE log (
    id SERIAL PRIMARY KEY,
    context VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    datetime TIMESTAMP NOT NULL,
    severity INTEGER NOT NULL,
    http_verb VARCHAR NULL,
    request_url VARCHAR NULL,
    authenticated_user_id INTEGER NULL
);
