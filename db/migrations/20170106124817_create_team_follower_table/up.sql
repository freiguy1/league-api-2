CREATE TABLE team_follower (
    id SERIAL PRIMARY KEY,
    team_id INTEGER NOT NULL REFERENCES team(id),
    user_id INTEGER NOT NULL REFERENCES league_user(id),
    UNIQUE (team_id, user_id)
);
