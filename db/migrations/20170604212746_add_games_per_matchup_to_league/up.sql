ALTER TABLE league ADD COLUMN games_per_matchup INTEGER;

UPDATE league SET games_per_matchup = 1;

ALTER TABLE league ALTER COLUMN games_per_matchup SET NOT NULL;
