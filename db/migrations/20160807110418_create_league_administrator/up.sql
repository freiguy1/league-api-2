CREATE TABLE league_administrator (
    id SERIAL PRIMARY KEY,
    league_id INTEGER NOT NULL REFERENCES league,
    user_id INTEGER NOT NULL REFERENCES league_user
);

ALTER TABLE league
    DROP COLUMN contact_email,
    DROP COLUMN contact_phone;
