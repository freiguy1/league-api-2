CREATE TABLE user_password (
    id SERIAL PRIMARY KEY,
    password VARCHAR NOT NULL,
    salt VARCHAR NOT NULL
);
