CREATE TABLE team_event (
    id SERIAL PRIMARY KEY,
    datetime TIMESTAMP NOT NULL,
    league_id INTEGER NOT NULL REFERENCES league(id),
    notes TEXT NULL
);

DROP TABLE game;

CREATE TABLE game (
    team_event_id INTEGER PRIMARY KEY REFERENCES team_event(id),
    game_location_id INTEGER NOT NULL REFERENCES game_location(id),
    home_team_id INTEGER NOT NULL REFERENCES team(id),
    away_team_id INTEGER NOT NULL REFERENCES team(id),
    game_status_id INTEGER NOT NULL REFERENCES game_status(id),
    rescheduled_team_event_id INTEGER NULL REFERENCES team_event(id),
    home_team_score INTEGER NULL,
    away_team_score INTEGER NULL
);

CREATE TABLE bye (
    team_event_id INTEGER PRIMARY KEY REFERENCES team_event(id),
    team_id INTEGER NOT NULL REFERENCES team(id)
);
