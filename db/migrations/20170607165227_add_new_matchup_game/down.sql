DROP TABLE IF EXISTS game;

ALTER TABLE matchup ADD COLUMN away_team_score INTEGER;
ALTER TABLE matchup ADD COLUMN home_team_score INTEGER;
