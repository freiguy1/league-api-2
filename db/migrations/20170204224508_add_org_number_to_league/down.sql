
ALTER TABLE league DROP COLUMN IF EXISTS organization;

ALTER TABLE league DROP COLUMN IF EXISTS contact_phone;
