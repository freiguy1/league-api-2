CREATE TABLE oauth2_info (
    id SERIAL PRIMARY KEY,
    provider VARCHAR NOT NULL,
    profile_id VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    user_id INTEGER NULL REFERENCES league_user(id)
);
