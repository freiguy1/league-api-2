ALTER TABLE matchup_location RENAME TO game_location;

ALTER TABLE matchup_status RENAME TO game_status;

ALTER TABLE matchup RENAME COLUMN matchup_location_id TO game_location_id;

ALTER TABLE matchup RENAME COLUMN matchup_status_id TO game_status_id;

ALTER TABLE matchup RENAME TO game;
