ALTER TABLE game_location RENAME TO matchup_location;

ALTER TABLE game_status RENAME TO matchup_status;

ALTER TABLE game RENAME COLUMN game_location_id TO matchup_location_id;

ALTER TABLE game RENAME COLUMN game_status_id TO matchup_status_id;

ALTER TABLE game RENAME TO matchup;

